var files =
[
    [ "BazaDanych.cpp", "_baza_danych_8cpp.html", null ],
    [ "BazaDanych.hpp", "_baza_danych_8hpp.html", [
      [ "BazaDanych", "class_baza_danych.html", "class_baza_danych" ]
    ] ],
    [ "main.cpp", "main_8cpp.html", "main_8cpp" ],
    [ "OknoGlowne.cpp", "_okno_glowne_8cpp.html", null ],
    [ "OknoGlowne.hpp", "_okno_glowne_8hpp.html", [
      [ "OknoGlowne", "class_okno_glowne.html", "class_okno_glowne" ]
    ] ],
    [ "PomiaryTablica.cpp", "_pomiary_tablica_8cpp.html", null ],
    [ "PomiaryTablica.hpp", "_pomiary_tablica_8hpp.html", [
      [ "WpisPomiarowy", "struct_wpis_pomiarowy.html", "struct_wpis_pomiarowy" ],
      [ "PomiaryTablica", "class_pomiary_tablica.html", "class_pomiary_tablica" ]
    ] ],
    [ "StrPom.cpp", "_str_pom_8cpp.html", null ],
    [ "StrPom.hpp", "_str_pom_8hpp.html", "_str_pom_8hpp" ],
    [ "WatekBazyDanych.cpp", "_watek_bazy_danych_8cpp.html", null ],
    [ "WatekBazyDanych.hpp", "_watek_bazy_danych_8hpp.html", [
      [ "WatekBazyDanych", "class_watek_bazy_danych.html", "class_watek_bazy_danych" ]
    ] ],
    [ "WpisPomiarowy.cpp", "_wpis_pomiarowy_8cpp.html", null ],
    [ "WykresCisn.cpp", "_wykres_cisn_8cpp.html", null ],
    [ "WykresCisn.hpp", "_wykres_cisn_8hpp.html", [
      [ "WykresCisn", "class_wykres_cisn.html", "class_wykres_cisn" ]
    ] ],
    [ "WykresTemp.cpp", "_wykres_temp_8cpp.html", null ],
    [ "WykresTemp.hpp", "_wykres_temp_8hpp.html", [
      [ "WykresTemp", "class_wykres_temp.html", "class_wykres_temp" ]
    ] ],
    [ "WykresWidok.cpp", "_wykres_widok_8cpp.html", null ],
    [ "WykresWidok.hpp", "_wykres_widok_8hpp.html", [
      [ "WykresWidok", "class_wykres_widok.html", "class_wykres_widok" ]
    ] ],
    [ "WykresWilg.cpp", "_wykres_wilg_8cpp.html", null ],
    [ "WykresWilg.hpp", "_wykres_wilg_8hpp.html", [
      [ "WykresWilg", "class_wykres_wilg.html", "class_wykres_wilg" ]
    ] ],
    [ "ZakladkaCisn.cpp", "_zakladka_cisn_8cpp.html", null ],
    [ "ZakladkaCisn.hpp", "_zakladka_cisn_8hpp.html", [
      [ "ZakladkaCisn", "class_zakladka_cisn.html", "class_zakladka_cisn" ]
    ] ],
    [ "ZakladkaOgolne.cpp", "_zakladka_ogolne_8cpp.html", null ],
    [ "ZakladkaOgolne.hpp", "_zakladka_ogolne_8hpp.html", [
      [ "ZakladkaOgolne", "class_zakladka_ogolne.html", "class_zakladka_ogolne" ]
    ] ],
    [ "ZakladkaTemp.cpp", "_zakladka_temp_8cpp.html", null ],
    [ "ZakladkaTemp.hpp", "_zakladka_temp_8hpp.html", [
      [ "ZakladkaTemp", "class_zakladka_temp.html", "class_zakladka_temp" ]
    ] ],
    [ "ZakladkaWilg.cpp", "_zakladka_wilg_8cpp.html", null ],
    [ "ZakladkaWilg.hpp", "_zakladka_wilg_8hpp.html", [
      [ "ZakladkaWilg", "class_zakladka_wilg.html", "class_zakladka_wilg" ]
    ] ],
    [ "Zakladki.cpp", "_zakladki_8cpp.html", null ],
    [ "Zakladki.hpp", "_zakladki_8hpp.html", [
      [ "Zakladki", "class_zakladki.html", "class_zakladki" ]
    ] ]
];