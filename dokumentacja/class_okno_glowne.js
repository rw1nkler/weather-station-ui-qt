var class_okno_glowne =
[
    [ "OknoGlowne", "class_okno_glowne.html#a42be8f76b123b353a6708babe7eeae20", null ],
    [ "~OknoGlowne", "class_okno_glowne.html#a200ea763c5ce5feeac755c3488b5ce27", null ],
    [ "CzyMoznaZamknac", "class_okno_glowne.html#abb892269a6111e0c4cc26f702f6e4c6d", null ],
    [ "OdbierzNapisStatusu", "class_okno_glowne.html#a0efeaa91ee89cd43acd1000a98565954", null ],
    [ "OdbierzNapisStatusu", "class_okno_glowne.html#a9d0693ade434f8b88c0765ce7b75c3c0", null ],
    [ "UtworzAkcje", "class_okno_glowne.html#a919f0b325e254c4dd2f4914a452c0cea", null ],
    [ "UtworzMenu", "class_okno_glowne.html#af59a38c7f8081c926d2a6bb4734b4112", null ],
    [ "UtworzPasekStatusu", "class_okno_glowne.html#a862c4e82a12abc07994048fa0c6ae5e2", null ],
    [ "UtworzPasekZakladek", "class_okno_glowne.html#aec952cff1ef0419178f4c3805a6c37b4", null ],
    [ "WyswietlInfo", "class_okno_glowne.html#a086fc8253c20232d3cc08c52a22e8a94", null ],
    [ "WyswietlPomoc", "class_okno_glowne.html#a14fec34956b548a41e8fa6ca8fe70c00", null ],
    [ "_Baza", "class_okno_glowne.html#ac44067d9fb16035b3faa3d10365b66a5", null ],
    [ "_MutexGdyOdczytujePomiary", "class_okno_glowne.html#abe75fd4f98c50e2503fdd452b7537e3e", null ],
    [ "_Pomiary", "class_okno_glowne.html#ac1063533938ed8840468f130c8f53020", null ],
    [ "_wAkcjaInformacje", "class_okno_glowne.html#a768975fe6835c0b4862b7b362a6ec7f3", null ],
    [ "_wAkcjaPobierzDane", "class_okno_glowne.html#a921376207c79f139b29d4c79b40b6ffd", null ],
    [ "_wAkcjaPomoc", "class_okno_glowne.html#a5bbfbe6e1738d4e910a3ab214e7a63a5", null ],
    [ "_wAkcjaZamknij", "class_okno_glowne.html#a2a959751f9ec212e6fcdcc7afdf31c97", null ],
    [ "_wMenu", "class_okno_glowne.html#a13a12a71c1db120cb701cf323b530a4f", null ],
    [ "_wWatekBD", "class_okno_glowne.html#a42dceb4c4e496ad8111c9c424d20ddc8", null ],
    [ "_wZakladki", "class_okno_glowne.html#a8cadebd7c84c870a9098024872beae8b", null ]
];