var annotated_dup =
[
    [ "BazaDanych", "class_baza_danych.html", "class_baza_danych" ],
    [ "OknoGlowne", "class_okno_glowne.html", "class_okno_glowne" ],
    [ "PomiaryTablica", "class_pomiary_tablica.html", "class_pomiary_tablica" ],
    [ "StrPom", "class_str_pom.html", "class_str_pom" ],
    [ "WatekBazyDanych", "class_watek_bazy_danych.html", "class_watek_bazy_danych" ],
    [ "WpisPomiarowy", "struct_wpis_pomiarowy.html", "struct_wpis_pomiarowy" ],
    [ "WykresCisn", "class_wykres_cisn.html", "class_wykres_cisn" ],
    [ "WykresTemp", "class_wykres_temp.html", "class_wykres_temp" ],
    [ "WykresWidok", "class_wykres_widok.html", "class_wykres_widok" ],
    [ "WykresWilg", "class_wykres_wilg.html", "class_wykres_wilg" ],
    [ "ZakladkaCisn", "class_zakladka_cisn.html", "class_zakladka_cisn" ],
    [ "ZakladkaOgolne", "class_zakladka_ogolne.html", "class_zakladka_ogolne" ],
    [ "ZakladkaTemp", "class_zakladka_temp.html", "class_zakladka_temp" ],
    [ "ZakladkaWilg", "class_zakladka_wilg.html", "class_zakladka_wilg" ],
    [ "Zakladki", "class_zakladki.html", "class_zakladki" ]
];