var searchData=
[
  ['gdychcepredykcje',['GdyChcePredykcje',['../class_wykres_cisn.html#a8d84b3b3ac5e2a7568871940aefc5752',1,'WykresCisn::GdyChcePredykcje()'],['../class_wykres_temp.html#a6beb775c44b10b2e898f7fabe9d4931e',1,'WykresTemp::GdyChcePredykcje()'],['../class_wykres_wilg.html#aedeb7ce332d920a078384eeae383a91e',1,'WykresWilg::GdyChcePredykcje()']]],
  ['gdypobranodane',['GdyPobranoDane',['../class_zakladki.html#a036588c3c804b1cb8f3692294d176333',1,'Zakladki']]],
  ['gdyzatwierdzonowybor',['GdyZatwierdzonoWybor',['../class_zakladka_cisn.html#a0f4f9adf21c47b8c366cbb4234a70df1',1,'ZakladkaCisn::GdyZatwierdzonoWybor()'],['../class_zakladka_temp.html#aa0ecf0a5241b54ab315fa73bcacf5b99',1,'ZakladkaTemp::GdyZatwierdzonoWybor()'],['../class_zakladka_wilg.html#aa10502fe6b200491369d7b9f0a71ced4',1,'ZakladkaWilg::GdyZatwierdzonoWybor()']]],
  ['getnazwa',['getNazwa',['../class_wykres_cisn.html#ae12d1a2da94cfb33deffcd1421cc0045',1,'WykresCisn::getNazwa()'],['../class_wykres_temp.html#a2ca2d5748804265abaa7cccbe15e21df',1,'WykresTemp::getNazwa()'],['../class_wykres_wilg.html#a4882812705177299820e063870b10e99',1,'WykresWilg::getNazwa()']]],
  ['getsize',['GetSize',['../class_pomiary_tablica.html#ae47558cc0971ec632b7ba4dfc67a24e1',1,'PomiaryTablica']]],
  ['getswoknog',['GetswOknoG',['../class_okno_glowne.html#ad794c9e16573d08554f353220009ab80',1,'OknoGlowne']]],
  ['getwmutex',['GetwMutex',['../class_okno_glowne.html#ace20dbc355cade5c13f29304e7a18525',1,'OknoGlowne']]],
  ['getwpomiary',['GetwPomiary',['../class_okno_glowne.html#ae23b3ed429431b57446278f63a031a95',1,'OknoGlowne']]],
  ['getwstrpom',['getwStrPom',['../class_zakladki.html#ad3d667fc7edd9d9ee16cda3ce015c1aa',1,'Zakladki']]],
  ['getwwatekbd',['GetwWatekBD',['../class_okno_glowne.html#a9b801fa9d10950b00f0e1649acdd5d6d',1,'OknoGlowne']]],
  ['getwykres',['getWykres',['../class_wykres_cisn.html#a72cdd7e410f75cd8da2b82beb0734592',1,'WykresCisn::getWykres()'],['../class_wykres_temp.html#a58ad7d29270be1e787649aafa3c5d4b6',1,'WykresTemp::getWykres()'],['../class_wykres_wilg.html#a092eb8b4f6ecec4c8ee8d219399aa89d',1,'WykresWilg::getWykres()']]],
  ['getwykrescisn',['getWykresCisn',['../class_zakladka_ogolne.html#a85ddb82ffeadcb3a5e1fa08e78cdb46f',1,'ZakladkaOgolne']]],
  ['getwykrestemp',['getWykresTemp',['../class_zakladka_ogolne.html#abd768348d8693350596cfdc2d8a0fb6e',1,'ZakladkaOgolne']]],
  ['getwykreswilg',['getWykresWilg',['../class_zakladka_ogolne.html#a0f33b7ad3edc7e68337c47ec898577ca',1,'ZakladkaOgolne']]]
];
