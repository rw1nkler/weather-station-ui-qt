#include <QWidget>
#include <QLabel>
#include <QGroupBox>
#include <QVBoxLayout>
#include <QGridLayout>
#include <QFormLayout>
#include <QComboBox>
#include <iostream>
#include <QCalendarWidget>

#include "ZakladkaTemp.hpp"
#include "Zakladki.hpp"
#include "WykresTemp.hpp"
#include "OknoGlowne.hpp"

ZakladkaTemp::ZakladkaTemp(QWidget *wRodzic): QWidget(wRodzic){
  QVBoxLayout *wOrganizerWer1 = new QVBoxLayout;
  _wWykTempGroupBox = new QGroupBox(this);
  _wWykTemp         = new WykresTemp(&(OknoGlowne::GetwPomiary()->TempOkr), OknoGlowne::GetwPomiary()->ZTemp);
  _wWykTempWidok    = new WykresWidok(_wWykTemp->getWykres(), _wWykTempGroupBox);
    
  _wWykTempGroupBox->setTitle(tr("Wykres temperatury"));
  _wWykTempGroupBox->adjustSize();

  wOrganizerWer1->addWidget(_wWykTempWidok);
  _wWykTempGroupBox->setLayout(wOrganizerWer1);

  QGridLayout *wOrganizerGrid = new QGridLayout;

  _wInfoOgolneGroupBox = new QGroupBox(this);
  QFormLayout *wOrganizerForm1 = new QFormLayout;
  _wEditMaxTemp  = new QLineEdit(_wInfoOgolneGroupBox);
  _wEditMaxTemp->setReadOnly(true);
  _wEditMaxTemp->setAlignment(Qt::AlignHCenter);
  _wEditMinTemp   = new QLineEdit(_wInfoOgolneGroupBox);
  _wEditMinTemp->setReadOnly(true);
  _wEditMinTemp->setAlignment(Qt::AlignHCenter);
  _wEditAktTemp   = new QLineEdit(_wInfoOgolneGroupBox);
  _wEditAktTemp->setReadOnly(true);
  _wEditAktTemp->setAlignment(Qt::AlignHCenter);
  _wEditMaxDzisTemp   = new QLineEdit(_wInfoOgolneGroupBox);
  _wEditMaxDzisTemp->setReadOnly(true);
  _wEditMaxDzisTemp->setAlignment(Qt::AlignHCenter);
  _wEditMinDzisTemp   = new QLineEdit(_wInfoOgolneGroupBox);
  _wEditMinDzisTemp->setReadOnly(true);
  _wEditMinDzisTemp->setAlignment(Qt::AlignHCenter);

  wOrganizerForm1->addRow(new QLabel(tr("Zarejestrowane maksimum:")), _wEditMaxTemp);
  wOrganizerForm1->addRow(new QLabel(tr("Zarejestrowane minimum:")),  _wEditMinTemp);
  wOrganizerForm1->addRow(new QLabel(tr("Aktualna temperatura:")), _wEditAktTemp);
  wOrganizerForm1->addRow(new QLabel(tr("Maksimum dzisiaj:")), _wEditMaxDzisTemp);
  wOrganizerForm1->addRow(new QLabel(tr("Minimum dzisiaj:")), _wEditMinDzisTemp);
  _wInfoOgolneGroupBox->setLayout(wOrganizerForm1);
  _wInfoOgolneGroupBox->setTitle(tr("Podstawowe dane"));
  _wInfoOgolneGroupBox->adjustSize();

  _wDaneStatGroupBox = new QGroupBox(this);
  QFormLayout *wOrganizerForm2 = new QFormLayout;
  _wEditAvgTemp  = new QLineEdit(_wDaneStatGroupBox);
  _wEditAvgTemp->setReadOnly(true);
  _wEditAvgTemp->setAlignment(Qt::AlignHCenter);
  _wEditMedTemp   = new QLineEdit(_wDaneStatGroupBox);
  _wEditMedTemp->setReadOnly(true);
  _wEditMedTemp->setAlignment(Qt::AlignHCenter);
  _wEditStdTemp   = new QLineEdit(_wDaneStatGroupBox);
  _wEditStdTemp->setReadOnly(true);
  _wEditStdTemp->setAlignment(Qt::AlignHCenter);
  _wEditVarTemp   = new QLineEdit(_wDaneStatGroupBox);
  _wEditVarTemp->setReadOnly(true);
  _wEditVarTemp->setAlignment(Qt::AlignHCenter);

  wOrganizerForm2->addRow(new QLabel(tr("Średnia:")), _wEditAvgTemp);
  wOrganizerForm2->addRow(new QLabel(tr("Mediana:")),  _wEditMedTemp);
  wOrganizerForm2->addRow(new QLabel(tr("Odchylenie standardowe:")), _wEditStdTemp);
  wOrganizerForm2->addRow(new QLabel(tr("Wariancja:")), _wEditVarTemp);

  _wDaneStatGroupBox->setLayout(wOrganizerForm2);
  _wDaneStatGroupBox->setTitle(tr("Dane Statystyczne"));
  _wDaneStatGroupBox->adjustSize();

  _wPoleWyboruGroupBox = new QGroupBox(this);
  QGridLayout *wOrganizerGrid2 = new QGridLayout;
  _wComboBoxZakres = new QComboBox(_wPoleWyboruGroupBox);
  _wComboBoxZakres->addItem("Podaj Datę", 1);
  _wComboBoxZakres->addItem("Dzisiaj", 2);
  _wComboBoxZakres->addItem("Tydzień", 3);
  _wComboBoxZakres->addItem("Miesiąc", 4);
  _wLabComboBoxZakres = new QLabel("Albo zmień tryb wyświetlania:", _wPoleWyboruGroupBox);
  _wLabKalendarzOD = new QLabel("Wybierz datę początkową: ");
  _wLabKalendarzDO = new QLabel("Wybierz datę końcową: ");
  _wKalendarzOD = new QCalendarWidget;
  _wKalendarzOD->setMinimumDate(QDate(1900, 1, 1));
  _wKalendarzOD->setMaximumDate(QDate(3000, 1, 1));
  _wKalendarzOD->setGridVisible(true);
  _wKalendarzDO = new QCalendarWidget;
  _wKalendarzDO->setMinimumDate(QDate(1900, 1, 1));
  _wKalendarzDO->setMaximumDate(QDate(3000, 1, 1));
  _wKalendarzDO->setGridVisible(true);
  wOrganizerGrid2->addWidget(_wLabKalendarzOD, 0, 0, 1, 4);
  wOrganizerGrid2->addWidget(_wKalendarzOD, 1, 0, 4, 4);
  wOrganizerGrid2->addWidget(_wLabKalendarzDO, 5, 0, 1, 4);
  wOrganizerGrid2->addWidget(_wKalendarzDO, 6, 0, 4, 4);
  wOrganizerGrid2->addWidget(_wLabComboBoxZakres, 10, 0, 1, 4);

  _wZatwierdz = new QPushButton("Zatwierdź",_wPoleWyboruGroupBox);
  wOrganizerGrid2->addWidget(_wZatwierdz, 11, 3, 1, 1);

  wOrganizerGrid2->addWidget(_wComboBoxZakres, 11, 0, 1, 3);
  _wPoleWyboruGroupBox->setLayout(wOrganizerGrid2);

  _wPoleWyboruGroupBox->setTitle(tr("Zakres wyświetlania"));

  _wPredykcjaGroubBox = new QGroupBox(this);
  _wCheckBox = new QCheckBox(tr("metoda Browna"), _wPredykcjaGroubBox);
  QVBoxLayout *wOrganizerWer3 = new QVBoxLayout;
  wOrganizerWer3->addWidget(_wCheckBox);
  _wPredykcjaGroubBox->setLayout(wOrganizerWer3);
  _wPredykcjaGroubBox->setTitle(tr("Pokaż predykcję"));

  _wInfoOgolneGroupBox->setMaximumHeight(_wInfoOgolneGroupBox->height());
  _wDaneStatGroupBox->setMaximumHeight(_wInfoOgolneGroupBox->height());
  _wPoleWyboruGroupBox->setMinimumWidth(250);
  _wPredykcjaGroubBox->setMinimumWidth(250);

  wOrganizerGrid->addWidget(_wWykTempGroupBox, 0, 0, 4, 2);
  wOrganizerGrid->addWidget(_wInfoOgolneGroupBox, 4, 0, 2, 1);
  wOrganizerGrid->addWidget(_wDaneStatGroupBox, 4, 1, 2, 1);
  wOrganizerGrid->addWidget(_wPoleWyboruGroupBox, 0, 2, 5, 1);
  wOrganizerGrid->addWidget(_wPredykcjaGroubBox, 5, 2, 1, 1);
  wOrganizerGrid->setColumnStretch(0,1);
  wOrganizerGrid->setColumnStretch(1,1);
  wOrganizerGrid->setRowStretch(0,1);
  wOrganizerGrid->setRowStretch(1,1);
  wOrganizerGrid->setRowStretch(2,1);
  wOrganizerGrid->setRowStretch(3,1);

  this->setLayout(wOrganizerGrid);

  connect(_wCheckBox, SIGNAL(stateChanged(int)), _wWykTemp, SLOT(GdyChcePredykcje(int)));
  connect(_wZatwierdz, SIGNAL(clicked(bool)), this, SLOT(GdyZatwierdzonoWybor(bool)));
  connect(this, SIGNAL(PrzekazStatus(QString)), OknoGlowne::GetswOknoG(), SLOT(OdbierzNapisStatusu(QString)));

}

void ZakladkaTemp::uaktualnij(){
  _wWykTemp->uaktualnij();
  _wEditMaxTemp->setText(QString::number(OknoGlowne::GetwPomiary()->MaxT,'f',2) + tr(" °C"));
  _wEditMinTemp->setText(QString::number(OknoGlowne::GetwPomiary()->MinT,'f',2) + tr(" °C"));
  _wEditAktTemp->setText(QString::number(OknoGlowne::GetwPomiary()->AktT,'f',2) + tr(" °C"));
  _wEditMaxDzisTemp->setText(QString::number(OknoGlowne::GetwPomiary()->MaxTDzis,'f',2) + tr(" °C"));
  _wEditMinDzisTemp->setText(QString::number(OknoGlowne::GetwPomiary()->MinTDzis,'f',2) + tr(" °C"));
  _wEditAvgTemp->setText(QString::number(OknoGlowne::GetwPomiary()->AvgTOkr,'f',2) + tr(" °C"));
  _wEditMedTemp->setText(QString::number(OknoGlowne::GetwPomiary()->MedTemp(),'f',2) + tr(" °C"));
  _wEditStdTemp->setText(QString::number(OknoGlowne::GetwPomiary()->StdTOkr,'f',2) + tr(" °C"));
  _wEditVarTemp->setText(QString::number(OknoGlowne::GetwPomiary()->VarTOkr,'f',2) + tr(" °C"));
}

void ZakladkaTemp::GdyZatwierdzonoWybor(bool checked __attribute__((unused))){
    OknoGlowne::GetwMutex()->lock();
    OknoGlowne::GetwPomiary()->TempFlag = 1;
    switch(_wComboBoxZakres->currentIndex()){
      case 0:
        OknoGlowne::GetwPomiary()->ZTemp = Data;
        OknoGlowne::GetwPomiary()->DataOD = _wKalendarzOD->selectedDate();
        OknoGlowne::GetwPomiary()->DataDO = _wKalendarzDO->selectedDate();
        break;
      case 1:
        OknoGlowne::GetwPomiary()->ZTemp = Dzisiaj;
        break;
      case 2:
        OknoGlowne::GetwPomiary()->ZTemp = Tydzien;
        break;
      case 3:
        OknoGlowne::GetwPomiary()->ZTemp = Miesiac;
    }
    emit PrzekazStatus("Pytam baze");
    OknoGlowne::GetwWatekBD()->AsynPolaczenie();
    OknoGlowne::GetwMutex()->unlock();
    emit PrzekazStatus("Odpytalem");
}

