#include "StrPom.hpp"
#include <iostream>

using namespace std;

StrPom::StrPom(){
    TempFlag = 0;
    WilgFlag = 0;
    CisnFlag = 0;

    ZTemp = Dzisiaj;
    ZCisn = Dzisiaj;
    ZWilg = Dzisiaj;

    MaxTOkr = 0;
    MinTOkr = 0;
    AvgTOkr = 0;
    VarTOkr = 0;
    StdTOkr = 0;
    MaxTDzis = 0;
    MinTDzis = 0;

    AktT = 0;
    MaxT = 0;
    MinT = 0;

    MaxCOkr = 0;
    MinCOkr = 0;
    AvgCOkr = 0;
    VarCOkr = 0;
    StdCOkr = 0;
    MaxCDzis = 0;
    MinCDzis = 0;

    AktC = 0;
    MaxC = 0;
    MinC = 0;

    MaxWOkr = 0;
    MinWOkr = 0;
    AvgWOkr = 0;
    VarWOkr = 0;
    StdWOkr = 0;
    MaxWDzis = 0;
    MinWDzis = 0;

    AktW = 0;
    MaxW = 0;
    MinW = 0;

    QDate DataOD = QDate::currentDate();
    QDate DataDO = QDate::currentDate();
}

StrPom::~StrPom(){

}

double StrPom::MedTemp(){
    int i = TempOkr.GetSize()/2;
    if(TempOkr.GetSize() % 2 == 0){
       return (TempOkr.Tab(i-1).Wartosc+TempOkr.Tab(i).Wartosc)/2;
    }
    else return TempOkr.Tab(i).Wartosc;
}

double StrPom::MedCisn(){
    int i = CisnOkr.GetSize()/2;
    if(CisnOkr.GetSize() % 2 == 0){
       return (CisnOkr.Tab(i-1).Wartosc+CisnOkr.Tab(i).Wartosc)/2;
    }
    else return CisnOkr.Tab(i).Wartosc;
}

double StrPom::MedWilg(){
    int i = WilgOkr.GetSize()/2;
    if(WilgOkr.GetSize() % 2 == 0){
       return (WilgOkr.Tab(i-1).Wartosc+WilgOkr.Tab(i).Wartosc)/2;
    }
    else return WilgOkr.Tab(i).Wartosc;
}


void StrPom::WysStruk(){
    for(int i = 0; i < TempDzis.GetSize(); ++i){
        cout << "TempDzis[" << i << "]: " << TempDzis.Tab(i).Wartosc << endl;
    }
    cout << endl;

    for(int i = 0; i < TempOkr.GetSize(); ++i){
        cout << "TempDzis[" << i << "]: " << TempOkr.Tab(i).Wartosc << endl;
    }
    cout << endl;

    cout << "MaxT: " << MaxT << endl;
    cout << "MaxTDzis: " << MaxTDzis << endl;
    cout << "MaxTOkr: " << MaxTOkr << endl;

    cout << "MinT: " << MinT << endl;
    cout << "MinTDzis: " << MinTDzis << endl;
    cout << "MinTOkr: " << MinTOkr << endl;

    cout << "AktT: " << AktT << endl;

    for(int i = 0; i < CisnDzis.GetSize(); ++i){
        cout << "[" << i << "]: " << CisnDzis.Tab(i).Wartosc << endl;
    }
    cout << endl;

    for(int i = 0; i < CisnOkr.GetSize(); ++i){
        cout << "[" << i << "]: " << CisnOkr.Tab(i).Wartosc << endl;
    }
    cout << endl;

    cout << "MaxC: " << MaxC << endl;
    cout << "MaxCDzis: " << MaxCDzis << endl;
    cout << "MaxCOkr: " << MaxCOkr << endl;

    cout << "MinC: " << MinC << endl;
    cout << "MinCDzis: " << MinCDzis << endl;
    cout << "MinCOkr: " << MinCOkr << endl;

    cout << "AktC: " << AktC << endl;
}
