#include <QWidget>
#include <QLabel>
#include <QGroupBox>
#include <QVBoxLayout>
#include <QGridLayout>
#include <QFormLayout>
#include <QComboBox>

#include "ZakladkaCisn.hpp"
#include "Zakladki.hpp"
#include "WykresCisn.hpp"
#include "OknoGlowne.hpp"

ZakladkaCisn::ZakladkaCisn(QWidget *wRodzic): QWidget(wRodzic){
    QVBoxLayout *wOrganizerWer1 = new QVBoxLayout;
    _wWykCisnGroupBox = new QGroupBox(this);
    _wWykCisn         = new WykresCisn(&(OknoGlowne::GetwPomiary()->CisnOkr), OknoGlowne::GetwPomiary()->ZCisn);
    _wWykCisnWidok    = new WykresWidok(_wWykCisn->getWykres(), _wWykCisnGroupBox);

    _wWykCisnGroupBox->setTitle(tr("Wykres Cisneratury"));
    _wWykCisnGroupBox->adjustSize();

    wOrganizerWer1->addWidget(_wWykCisnWidok);
    _wWykCisnGroupBox->setLayout(wOrganizerWer1);

    QGridLayout *wOrganizerGrid = new QGridLayout;

    _wInfoOgolneGroupBox = new QGroupBox(this);
    QFormLayout *wOrganizerForm1 = new QFormLayout;
    _wEditMaxCisn  = new QLineEdit(_wInfoOgolneGroupBox);
    _wEditMaxCisn->setReadOnly(true);
    _wEditMaxCisn->setAlignment(Qt::AlignHCenter);
    _wEditMinCisn   = new QLineEdit(_wInfoOgolneGroupBox);
    _wEditMinCisn->setReadOnly(true);
    _wEditMinCisn->setAlignment(Qt::AlignHCenter);
    _wEditAktCisn   = new QLineEdit(_wInfoOgolneGroupBox);
    _wEditAktCisn->setReadOnly(true);
    _wEditAktCisn->setAlignment(Qt::AlignHCenter);
    _wEditMaxDzisCisn   = new QLineEdit(_wInfoOgolneGroupBox);
    _wEditMaxDzisCisn->setReadOnly(true);
    _wEditMaxDzisCisn->setAlignment(Qt::AlignHCenter);
    _wEditMinDzisCisn   = new QLineEdit(_wInfoOgolneGroupBox);
    _wEditMinDzisCisn->setReadOnly(true);
    _wEditMinDzisCisn->setAlignment(Qt::AlignHCenter);

    wOrganizerForm1->addRow(new QLabel(tr("Zarejestrowane maksimum:")), _wEditMaxCisn);
    wOrganizerForm1->addRow(new QLabel(tr("Zarejestrowane minimum:")),  _wEditMinCisn);
    wOrganizerForm1->addRow(new QLabel(tr("Aktualna Cisneratura:")), _wEditAktCisn);
    wOrganizerForm1->addRow(new QLabel(tr("Maksimum dzisiaj:")), _wEditMaxDzisCisn);
    wOrganizerForm1->addRow(new QLabel(tr("Minimum dzisiaj:")), _wEditMinDzisCisn);
    _wInfoOgolneGroupBox->setLayout(wOrganizerForm1);
    _wInfoOgolneGroupBox->setTitle(tr("Podstawowe dane"));
    _wInfoOgolneGroupBox->adjustSize();

    _wDaneStatGroupBox = new QGroupBox(this);
    QFormLayout *wOrganizerForm2 = new QFormLayout;
    _wEditAvgCisn  = new QLineEdit(_wDaneStatGroupBox);
    _wEditAvgCisn->setReadOnly(true);
    _wEditAvgCisn->setAlignment(Qt::AlignHCenter);
    _wEditMedCisn   = new QLineEdit(_wDaneStatGroupBox);
    _wEditMedCisn->setReadOnly(true);
    _wEditMedCisn->setAlignment(Qt::AlignHCenter);
    _wEditStdCisn   = new QLineEdit(_wDaneStatGroupBox);
    _wEditStdCisn->setReadOnly(true);
    _wEditStdCisn->setAlignment(Qt::AlignHCenter);
    _wEditVarCisn   = new QLineEdit(_wDaneStatGroupBox);
    _wEditVarCisn->setReadOnly(true);
    _wEditVarCisn->setAlignment(Qt::AlignHCenter);

    wOrganizerForm2->addRow(new QLabel(tr("Średnia:")), _wEditAvgCisn);
    wOrganizerForm2->addRow(new QLabel(tr("Mediana:")),  _wEditMedCisn);
    wOrganizerForm2->addRow(new QLabel(tr("Odchylenie standardowe:")), _wEditStdCisn);
    wOrganizerForm2->addRow(new QLabel(tr("Wariancja:")), _wEditVarCisn);

    _wDaneStatGroupBox->setLayout(wOrganizerForm2);
    _wDaneStatGroupBox->setTitle(tr("Dane Statystyczne"));
    _wDaneStatGroupBox->adjustSize();

    _wPoleWyboruGroupBox = new QGroupBox(this);
    QGridLayout *wOrganizerGrid2 = new QGridLayout;
    _wComboBoxZakres = new QComboBox(_wPoleWyboruGroupBox);
    _wComboBoxZakres->addItem("Podaj Datę", 1);
    _wComboBoxZakres->addItem("Dzisiaj", 2);
    _wComboBoxZakres->addItem("Tydzień", 3);
    _wComboBoxZakres->addItem("Miesiąc", 4);
    _wLabComboBoxZakres = new QLabel("Albo zmień tryb wyświetlania:", _wPoleWyboruGroupBox);
    _wLabKalendarzOD = new QLabel("Wybierz datę początkową: ");
    _wLabKalendarzDO = new QLabel("Wybierz datę końcową: ");
    _wKalendarzOD = new QCalendarWidget;
    _wKalendarzOD->setMinimumDate(QDate(1900, 1, 1));
    _wKalendarzOD->setMaximumDate(QDate(3000, 1, 1));
    _wKalendarzOD->setGridVisible(true);
    _wKalendarzDO = new QCalendarWidget;
    _wKalendarzDO->setMinimumDate(QDate(1900, 1, 1));
    _wKalendarzDO->setMaximumDate(QDate(3000, 1, 1));
    _wKalendarzDO->setGridVisible(true);
    wOrganizerGrid2->addWidget(_wLabKalendarzOD, 0, 0, 1, 4);
    wOrganizerGrid2->addWidget(_wKalendarzOD, 1, 0, 4, 4);
    wOrganizerGrid2->addWidget(_wLabKalendarzDO, 5, 0, 1, 4);
    wOrganizerGrid2->addWidget(_wKalendarzDO, 6, 0, 4, 4);
    wOrganizerGrid2->addWidget(_wLabComboBoxZakres, 10, 0, 1, 4);

    _wZatwierdz = new QPushButton("Zatwierdź",_wPoleWyboruGroupBox);
    wOrganizerGrid2->addWidget(_wZatwierdz, 11, 3, 1, 1);

    wOrganizerGrid2->addWidget(_wComboBoxZakres, 11, 0, 1, 3);
    _wPoleWyboruGroupBox->setLayout(wOrganizerGrid2);

    _wPoleWyboruGroupBox->setTitle(tr("Zakres wyświetlania"));

    _wPredykcjaGroubBox = new QGroupBox(this);
    _wCheckBox = new QCheckBox(tr("metoda Browna"), _wPredykcjaGroubBox);
    QVBoxLayout *wOrganizerWer3 = new QVBoxLayout;
    wOrganizerWer3->addWidget(_wCheckBox);
    _wPredykcjaGroubBox->setLayout(wOrganizerWer3);
    _wPredykcjaGroubBox->setTitle(tr("Pokaż predykcję"));

    _wInfoOgolneGroupBox->setMaximumHeight(_wInfoOgolneGroupBox->height());
    _wDaneStatGroupBox->setMaximumHeight(_wInfoOgolneGroupBox->height());
    _wPoleWyboruGroupBox->setMinimumWidth(250);
    _wPredykcjaGroubBox->setMinimumWidth(250);

    wOrganizerGrid->addWidget(_wWykCisnGroupBox, 0, 0, 4, 2);
    wOrganizerGrid->addWidget(_wInfoOgolneGroupBox, 4, 0, 2, 1);
    wOrganizerGrid->addWidget(_wDaneStatGroupBox, 4, 1, 2, 1);
    wOrganizerGrid->addWidget(_wPoleWyboruGroupBox, 0, 2, 5, 1);
    wOrganizerGrid->addWidget(_wPredykcjaGroubBox, 5, 2, 1, 1);
    wOrganizerGrid->setColumnStretch(0,1);
    wOrganizerGrid->setColumnStretch(1,1);
    wOrganizerGrid->setRowStretch(0,1);
    wOrganizerGrid->setRowStretch(1,1);
    wOrganizerGrid->setRowStretch(2,1);
    wOrganizerGrid->setRowStretch(3,1);

    this->setLayout(wOrganizerGrid);

    connect(_wCheckBox, SIGNAL(stateChanged(int)), _wWykCisn, SLOT(GdyChcePredykcje(int)));
    connect(_wZatwierdz, SIGNAL(clicked(bool)), this, SLOT(GdyZatwierdzonoWybor(bool)));
    connect(this, SIGNAL(PrzekazStatus(QString)), OknoGlowne::GetswOknoG(), SLOT(OdbierzNapisStatusu(QString)));

}

void ZakladkaCisn::uaktualnij(){
  _wWykCisn->uaktualnij();
  _wEditMaxCisn->setText(QString::number(OknoGlowne::GetwPomiary()->MaxC,'f',2) + " hPa");
  _wEditMinCisn->setText(QString::number(OknoGlowne::GetwPomiary()->MinC,'f',2) + " hPa");
  _wEditAktCisn->setText(QString::number(OknoGlowne::GetwPomiary()->AktC,'f',2) + " hPa");
  _wEditMaxDzisCisn->setText(QString::number(OknoGlowne::GetwPomiary()->MaxCDzis,'f',2) + " hPa");
  _wEditMinDzisCisn->setText(QString::number(OknoGlowne::GetwPomiary()->MinCDzis,'f',2) + " hPa");
  _wEditAvgCisn->setText(QString::number(OknoGlowne::GetwPomiary()->AvgCOkr,'f',2) + " hPa");
  _wEditMedCisn->setText(QString::number(OknoGlowne::GetwPomiary()->MedCisn(),'f',2) + " hPa");
  _wEditStdCisn->setText(QString::number(OknoGlowne::GetwPomiary()->StdCOkr,'f',2) + " hPa");
  _wEditVarCisn->setText(QString::number(OknoGlowne::GetwPomiary()->VarCOkr,'f',2) + " hPa");
}

void ZakladkaCisn::GdyZatwierdzonoWybor(bool checked __attribute__((unused))){
    OknoGlowne::GetwPomiary()->CisnFlag = 1;
    switch(_wComboBoxZakres->currentIndex()){
      case 0:
        OknoGlowne::GetwPomiary()->ZCisn = Data;
        OknoGlowne::GetwPomiary()->DataOD = _wKalendarzOD->selectedDate();
        OknoGlowne::GetwPomiary()->DataDO = _wKalendarzDO->selectedDate();
        break;
      case 1:
        OknoGlowne::GetwPomiary()->ZCisn = Dzisiaj;
        break;
      case 2:
        OknoGlowne::GetwPomiary()->ZCisn = Tydzien;
        break;
      case 3:
        OknoGlowne::GetwPomiary()->ZCisn = Miesiac;
    }
    emit PrzekazStatus("Pytam baze");
    OknoGlowne::GetwMutex()->lock();
    OknoGlowne::GetwWatekBD()->AsynPolaczenie();
    OknoGlowne::GetwMutex()->unlock();
    emit PrzekazStatus("Odpytalem");
}
