#include "BazaDanych.hpp"

#include <iostream>
#include <QDateTime>
#include <QString>

/*!
 * \file
 * \brief Zawiera źródła klasy BazaDanych
 *
 * Plik źródłowy klasy BazaDanych. Zawiera ona informację dotyczące połączenia
 * z bazą danych oraz metody, które umożliwiają pobranie danych z serwisu.
 *
 */

using namespace std;

/*!
 * Metoda ustawia w strukturze QSqlDatabase informacje niezbędne dla poprawnego połączenia z bazą danych, takie jak
 * rodzaj bazy danych, adres hosta, nazwa bazy danych, nazwa uzytkownika i jego hasło.
 * Dodatkowo ustawiane jest pole związane z ID czujnika.
 */
BazaDanych::BazaDanych(){
  db = QSqlDatabase::addDatabase("QMYSQL");      // Ustawienie informacji dotyczących polączenia
  //db.setHostName("192.168.1.20");
  db.setHostName("rwinkler.ddns.net");
  db.setDatabaseName("stacja_meteorologiczna");
  db.setUserName("robert");
  db.setPassword("rabarbar123");

  _CzujnikID = 15;                               // Ustawienie ID czujnika
}

/*!
 * Metoda sprawdza jaki rodzaj zapytania ma byc wykonany na podstawie typu wyliczeniowego IleDni
 * i wywołuje odpowiednią prywatną metodę pomocniczą obsługującą dany rodzaj zapytania.
 *
 * \param[in] wPomiary - Wskaźnik na strukturę przechowjąca dane pomiarowe i rodzaj zapytaniaa, które ma być sierowane do bazy danych.
 *
 * \retval 0  - Jeżeli połączenie z bazą danych zakończy się sukcesem.
 * \retval -1 - Jeżeli nie uda się uzyskać odpowiedzi od bazy danych.
 */
int BazaDanych::PobTemp(StrPom *wPomiary){
    switch(wPomiary->ZTemp){                  // Sprawdzenie jaki rodzaj zapytania ma być wykonany
      case Data:
        PobTemp_Data(wPomiary);               // Uruchomienie metody wykonujacej zapytanie typu zakres dni
        return 0;
        break;
      case Dzisiaj:
      case Tydzien:
      case Miesiac:
        PobTemp_Dni(wPomiary);                // Uruchomienie metody wykonujacej zapytanie typu n dni wstecz
        return 0;
        break;
      default:
        return -1;
    }
    return 0;
}

int BazaDanych::PobCisn(StrPom *wPomiary){
    switch(wPomiary->ZCisn){                  // Sprawdzenie jaki rodzaj zapytania ma być wykonany
      case Data:
        PobCisn_Data(wPomiary);               // Uruchomienie metody wykonujacej zapytanie typu zakres dni
        return 0;
        break;
      case Dzisiaj:
      case Tydzien:
      case Miesiac:
        PobCisn_Dni(wPomiary);                // Uruchomienie metody wykonujacej zapytanie typu n dni wstecz
        return 0;
        break;
      default:
        return -1;
    }
    return 0;
}

int BazaDanych::PobWilg(StrPom *wPomiary){
    switch(wPomiary->ZWilg){                  // Sprawdzenie jaki rodzaj zapytania ma być wykonany
      case Data:
        PobWilg_Data(wPomiary);               // Uruchomienie metody wykonujacej zapytanie typu zakres dni
        return 0;
        break;
      case Dzisiaj:
      case Tydzien:
      case Miesiac:
        PobWilg_Dni(wPomiary);                // Uruchomienie metody wykonujacej zapytanie typu n dni wstecz
        return 0;
        break;
      default:
        return -1;
    }
    return 0;
}

int BazaDanych::PobWilg_Dni(StrPom *wPomiary){
    int result = 0;
    if(!db.open()){
      cout << db.lastError().text().toStdString() << endl;
      return -1;
    }
    else{
      QString Zapytanie = "DHT11WILG";

      QSqlQuery QueryPomiary_Okr;
      QString SklZapPomiary_Okr;

      QSqlQuery QueryPomiary_Dzien;
      QString SklZapPomiary_Dzien;

      QSqlQuery QueryMaxW_Okr;
      QString SklZapMaxW_Okr;

      QSqlQuery QueryMaxW_Dzien;
      QString SklZapMaxW_Dzien;

      QSqlQuery QueryMinW_Okr;
      QString SklZapMinW_Okr;

      QSqlQuery QueryMinW_Dzien;
      QString SklZapMinW_Dzien;

      QSqlQuery QueryMaxW;
      QString SklZapMaxW;

      QSqlQuery QueryMinW;
      QString SklZapMinW;

      QSqlQuery QueryAvgW_Okr;
      QString SklZapAvgW_Okr;

      QSqlQuery QueryVarW_Okr;
      QString SklZapVarW_Okr;

      QSqlQuery QueryStdW_Okr;
      QString SklZapStdW_Okr;


      int Dni = 1;
      switch(wPomiary->ZWilg){
        case Dzisiaj:
          Dni = 1;
          break;
        case Tydzien:
          Dni = 7;
          break;
        case Miesiac:
          Dni = 30;
          break;
        default:
          db.close();
          cerr << "Zle podany zakres dni do wyswietlenia w BazaDanych::PobTemp_Dni()" << endl;
          return -1;
      }

      SklZapPomiary_Okr = "CALL pPomiary(" + QString::number(_CzujnikID) +", '" + Zapytanie + "', " + QString::number(Dni) + ");";
      cout << SklZapPomiary_Okr.toStdString() << endl;
      QueryPomiary_Okr.exec(SklZapPomiary_Okr);

      SklZapPomiary_Dzien = "CALL pPomiary(" + QString::number(_CzujnikID) +", '"+ Zapytanie +"', 1);";
      cout << SklZapPomiary_Dzien.toStdString() << endl;
      QueryPomiary_Dzien.exec(SklZapPomiary_Dzien);

      SklZapMaxW_Okr = "SELECT fMAX_DaysEarlier(" + QString::number(_CzujnikID) +", '"+ Zapytanie +"', " + QString::number(Dni) + ");";
      cout << SklZapMaxW_Okr.toStdString() << endl;
      QueryMaxW_Okr.exec(SklZapMaxW_Okr);

      SklZapMaxW_Dzien = "SELECT fMAX_DaysEarlier(" + QString::number(_CzujnikID) +", '"+ Zapytanie +"', 1);";
      cout << SklZapMaxW_Dzien.toStdString() << endl;
      QueryMaxW_Dzien.exec(SklZapMaxW_Dzien);

      SklZapMinW_Okr = "SELECT fMIN_DaysEarlier(" + QString::number(_CzujnikID) +", '"+ Zapytanie +"', " + QString::number(Dni) + ");";
      cout << SklZapMinW_Okr.toStdString() << endl;
      QueryMinW_Okr.exec(SklZapMinW_Okr);

      SklZapMinW_Dzien = "SELECT fMIN_DaysEarlier(" + QString::number(_CzujnikID) +", '"+ Zapytanie +"', 1);";
      cout << SklZapMinW_Dzien.toStdString() << endl;
      QueryMinW_Dzien.exec(SklZapMinW_Dzien);

      SklZapMaxW = "SELECT fMAX(" + QString::number(_CzujnikID) +", '"+ Zapytanie +"');";
      cout << SklZapMaxW.toStdString() << endl;
      QueryMaxW.exec(SklZapMaxW);

      SklZapMinW = "SELECT fMIN(" + QString::number(_CzujnikID) +", '"+ Zapytanie +"');";
      cout << SklZapMinW.toStdString() << endl;
      QueryMinW.exec(SklZapMinW);

      SklZapAvgW_Okr = "SELECT fAVG_DaysEarlier(" + QString::number(_CzujnikID) +", '"+ Zapytanie +"', " + QString::number(Dni) + ");";
      cout << SklZapAvgW_Okr.toStdString() << endl;
      QueryAvgW_Okr.exec(SklZapAvgW_Okr);

      SklZapVarW_Okr = "SELECT fVAR_DaysEarlier(" + QString::number(_CzujnikID) +", '"+ Zapytanie +"', " + QString::number(Dni) + ");";
      cout << SklZapVarW_Okr.toStdString() << endl;
      QueryVarW_Okr.exec(SklZapVarW_Okr);

      SklZapStdW_Okr = "SELECT fSTD_DaysEarlier(" + QString::number(_CzujnikID) +", '"+ Zapytanie +"', " + QString::number(Dni) + ");";
      cout << SklZapStdW_Okr.toStdString() << endl;
      QueryStdW_Okr.exec(SklZapStdW_Okr);


      int i = 0;
      if(QueryPomiary_Okr.size() != -1){
          wPomiary->WilgOkr.ReallocateTab(QueryPomiary_Okr.size());
          while(QueryPomiary_Okr.next()){
            wPomiary->WilgOkr.Tab(i).Czas = QueryPomiary_Okr.value(1).toTime();   //cout << Pomiary->TempOkr.Tab(i).Czas.toString("hh:mm:ss").toStdString() << endl;
            wPomiary->WilgOkr.Tab(i).Data = QueryPomiary_Okr.value(2).toDate();   //cout << Pomiary->TempOkr.Tab(i).Data.toString("yyyy-MM-dd").toStdString() << endl;
            wPomiary->WilgOkr.Tab(i).Wartosc = QueryPomiary_Okr.value(5).toDouble(); //cout << Pomiary->TempOkr.Tab(i).Wartosc << endl;
            ++i;
          }
      }
      else result = -1;
      i = 0;
      if(QueryPomiary_Dzien.size() != -1){
          wPomiary->WilgDzis.ReallocateTab(QueryPomiary_Dzien.size());
          while(QueryPomiary_Dzien.next()){
            wPomiary->WilgDzis.Tab(i).Czas = QueryPomiary_Dzien.value(1).toTime();   //cout << Pomiary->TempDzis.Tab(i).Czas.toString("hh:mm:ss").toStdString() << endl;
            wPomiary->WilgDzis.Tab(i).Data = QueryPomiary_Dzien.value(2).toDate();   //cout << Pomiary->TempDzis.Tab(i).Data.toString("yyyy-MM-dd").toStdString() << endl;
            wPomiary->WilgDzis.Tab(i).Wartosc = QueryPomiary_Dzien.value(5).toDouble(); //cout << Pomiary->TempDzis.Tab(i).Wartosc << endl;
            ++i;
          }
      }
      else result = -1;


      wPomiary->AktW = wPomiary->WilgDzis.Tab(i-1).Wartosc; //cout << Pomiary->AktT << endl;
      if(QueryMaxW_Okr.first())   wPomiary->MaxWOkr  = QueryMaxW_Okr.value(0).toDouble();   else result = -1;
      if(QueryMaxW_Dzien.first()) wPomiary->MaxWDzis = QueryMaxW_Dzien.value(0).toDouble(); else result = -1;
      if(QueryMinW_Okr.first())   wPomiary->MinWOkr  = QueryMinW_Okr.value(0).toDouble();   else result = -1;
      if(QueryMinW_Dzien.first()) wPomiary->MinWDzis = QueryMinW_Dzien.value(0).toDouble(); else result = -1;
      if(QueryMaxW.first()) wPomiary->MaxW = QueryMaxW.value(0).toDouble(); else result = -1;
      if(QueryMinW.first()) wPomiary->MinW = QueryMinW.value(0).toDouble(); else result = -1;
      if(QueryAvgW_Okr.first()) wPomiary->AvgWOkr = QueryAvgW_Okr.value(0).toDouble(); else result = -1;
      if(QueryVarW_Okr.first()) wPomiary->VarWOkr = QueryVarW_Okr.value(0).toDouble(); else result = -1;
      if(QueryStdW_Okr.first()) wPomiary->StdWOkr = QueryStdW_Okr.value(0).toDouble(); else result = -1;

      db.close();
    }
    return result;
}

/*!
 * Metoda wykonująca zapytanie pobierające dane o temperaturze n dni wstecz.
 * W zależności od wybranej wartości w polu Pomiary::ZTemp. Wykonuje kilka zapytań do bazy danych i pobiera następujące informacje:
 * - Pomiary temperatury z zadanego okresu
 * - Maksymalna temperatura z zadanego okresu
 * - Minimalna temperatura z zadanego okresu
 * - Średnia temperatura z zadanego okresu
 * - Variancja temperatury z zadanego okresu
 * - Odchylenie standardowe temperatury z zadanego okresu
 * - Maksymalna odnotowana temperatura
 * - Minimalna odnotowana temperatura
 * - Pomiary temperatury z dzisiaj
 * - Maksymalna temperatura z dzisiaj
 * - Minimalna temperatura z dzisiaj
 *
 * \param[in] wPomiary - Wskaźnik na strukturę przechowjąca dane pomiarowe i rodzaj zapytania, które ma być sierowane do bazy danych.
 *
 * \retval 0  - Jeżeli połączenie z bazą danych zakończy się sukcesem.
 * \retval -1 - Jeżeli nie uda się uzyskać odpowiedzi od bazy danych.
 */
int BazaDanych::PobTemp_Dni(StrPom* wPomiary){
  int result = 0;
  if(!db.open()){
    cout << db.lastError().text().toStdString() << endl;
    return -1;
  }
  else{
    QString Zapytanie = "BMP280TEMP";

    QSqlQuery QueryPomiary_Okr;
    QString SklZapPomiary_Okr;

    QSqlQuery QueryPomiary_Dzien;
    QString SklZapPomiary_Dzien;

    QSqlQuery QueryMaxT_Okr;
    QString SklZapMaxT_Okr;

    QSqlQuery QueryMaxT_Dzien;
    QString SklZapMaxT_Dzien;

    QSqlQuery QueryMinT_Okr;
    QString SklZapMinT_Okr;

    QSqlQuery QueryMinT_Dzien;
    QString SklZapMinT_Dzien;

    QSqlQuery QueryMaxT;
    QString SklZapMaxT;

    QSqlQuery QueryMinT;
    QString SklZapMinT;

    QSqlQuery QueryAvgT_Okr;
    QString SklZapAvgT_Okr;

    QSqlQuery QueryVarT_Okr;
    QString SklZapVarT_Okr;

    QSqlQuery QueryStdT_Okr;
    QString SklZapStdT_Okr;


    int Dni = 1;
    switch(wPomiary->ZTemp){
      case Dzisiaj:
        Dni = 1;
        break;
      case Tydzien:
        Dni = 7;
        break;
      case Miesiac:
        Dni = 30;
        break;
      default:
        db.close();
        cerr << "Zle podany zakres dni do wyswietlenia w BazaDanych::PobTemp_Dni()" << endl;
        return -1;
    }

    SklZapPomiary_Okr = "CALL pPomiary(" + QString::number(_CzujnikID) +", '" + Zapytanie + "', " + QString::number(Dni) + ");";
    cout << SklZapPomiary_Okr.toStdString() << endl;
    QueryPomiary_Okr.exec(SklZapPomiary_Okr);

    SklZapPomiary_Dzien = "CALL pPomiary(" + QString::number(_CzujnikID) +", '"+ Zapytanie +"', 1);";
    cout << SklZapPomiary_Dzien.toStdString() << endl;
    QueryPomiary_Dzien.exec(SklZapPomiary_Dzien);

    SklZapMaxT_Okr = "SELECT fMAX_DaysEarlier(" + QString::number(_CzujnikID) +", '"+ Zapytanie +"', " + QString::number(Dni) + ");";
    cout << SklZapMaxT_Okr.toStdString() << endl;
    QueryMaxT_Okr.exec(SklZapMaxT_Okr);

    SklZapMaxT_Dzien = "SELECT fMAX_DaysEarlier(" + QString::number(_CzujnikID) +", '"+ Zapytanie +"', 1);";
    cout << SklZapMaxT_Dzien.toStdString() << endl;
    QueryMaxT_Dzien.exec(SklZapMaxT_Dzien);

    SklZapMinT_Okr = "SELECT fMIN_DaysEarlier(" + QString::number(_CzujnikID) +", '"+ Zapytanie +"', " + QString::number(Dni) + ");";
    cout << SklZapMinT_Okr.toStdString() << endl;
    QueryMinT_Okr.exec(SklZapMinT_Okr);

    SklZapMinT_Dzien = "SELECT fMIN_DaysEarlier(" + QString::number(_CzujnikID) +", '"+ Zapytanie +"', 1);";
    cout << SklZapMinT_Dzien.toStdString() << endl;
    QueryMinT_Dzien.exec(SklZapMinT_Dzien);

    SklZapMaxT = "SELECT fMAX(" + QString::number(_CzujnikID) +", '"+ Zapytanie +"');";
    cout << SklZapMaxT.toStdString() << endl;
    QueryMaxT.exec(SklZapMaxT);

    SklZapMinT = "SELECT fMIN(" + QString::number(_CzujnikID) +", '"+ Zapytanie +"');";
    cout << SklZapMinT.toStdString() << endl;
    QueryMinT.exec(SklZapMinT);

    SklZapAvgT_Okr = "SELECT fAVG_DaysEarlier(" + QString::number(_CzujnikID) +", '"+ Zapytanie +"', " + QString::number(Dni) + ");";
    cout << SklZapAvgT_Okr.toStdString() << endl;
    QueryAvgT_Okr.exec(SklZapAvgT_Okr);

    SklZapVarT_Okr = "SELECT fVAR_DaysEarlier(" + QString::number(_CzujnikID) +", '"+ Zapytanie +"', " + QString::number(Dni) + ");";
    cout << SklZapVarT_Okr.toStdString() << endl;
    QueryVarT_Okr.exec(SklZapVarT_Okr);

    SklZapStdT_Okr = "SELECT fSTD_DaysEarlier(" + QString::number(_CzujnikID) +", '"+ Zapytanie +"', " + QString::number(Dni) + ");";
    cout << SklZapStdT_Okr.toStdString() << endl;
    QueryStdT_Okr.exec(SklZapStdT_Okr);


    int i = 0;
    if(QueryPomiary_Okr.size() != -1){
        wPomiary->TempOkr.ReallocateTab(QueryPomiary_Okr.size());
        while(QueryPomiary_Okr.next()){
          wPomiary->TempOkr.Tab(i).Czas = QueryPomiary_Okr.value(1).toTime();   //cout << Pomiary->TempOkr.Tab(i).Czas.toString("hh:mm:ss").toStdString() << endl;
          wPomiary->TempOkr.Tab(i).Data = QueryPomiary_Okr.value(2).toDate();   //cout << Pomiary->TempOkr.Tab(i).Data.toString("yyyy-MM-dd").toStdString() << endl;
          wPomiary->TempOkr.Tab(i).Wartosc = QueryPomiary_Okr.value(5).toDouble(); //cout << Pomiary->TempOkr.Tab(i).Wartosc << endl;
          ++i;
        }
    }
    else result = -1;
    i = 0;
    if(QueryPomiary_Dzien.size() != -1){
        wPomiary->TempDzis.ReallocateTab(QueryPomiary_Dzien.size());
        while(QueryPomiary_Dzien.next()){
          wPomiary->TempDzis.Tab(i).Czas = QueryPomiary_Dzien.value(1).toTime();   //cout << Pomiary->TempDzis.Tab(i).Czas.toString("hh:mm:ss").toStdString() << endl;
          wPomiary->TempDzis.Tab(i).Data = QueryPomiary_Dzien.value(2).toDate();   //cout << Pomiary->TempDzis.Tab(i).Data.toString("yyyy-MM-dd").toStdString() << endl;
          wPomiary->TempDzis.Tab(i).Wartosc = QueryPomiary_Dzien.value(5).toDouble(); //cout << Pomiary->TempDzis.Tab(i).Wartosc << endl;
          ++i;
        }
    }
    else result = -1;


    wPomiary->AktT = wPomiary->TempDzis.Tab(i-1).Wartosc; //cout << Pomiary->AktT << endl;
    if(QueryMaxT_Okr.first())   wPomiary->MaxTOkr  = QueryMaxT_Okr.value(0).toDouble();   else result = -1;
    if(QueryMaxT_Dzien.first()) wPomiary->MaxTDzis = QueryMaxT_Dzien.value(0).toDouble(); else result = -1;
    if(QueryMinT_Okr.first())   wPomiary->MinTOkr  = QueryMinT_Okr.value(0).toDouble();   else result = -1;
    if(QueryMinT_Dzien.first()) wPomiary->MinTDzis = QueryMinT_Dzien.value(0).toDouble(); else result = -1;
    if(QueryMaxT.first()) wPomiary->MaxT = QueryMaxT.value(0).toDouble(); else result = -1;
    if(QueryMinT.first()) wPomiary->MinT = QueryMinT.value(0).toDouble(); else result = -1;
    if(QueryAvgT_Okr.first()) wPomiary->AvgTOkr = QueryAvgT_Okr.value(0).toDouble(); else result = -1;
    if(QueryVarT_Okr.first()) wPomiary->VarTOkr = QueryVarT_Okr.value(0).toDouble(); else result = -1;
    if(QueryStdT_Okr.first()) wPomiary->StdTOkr = QueryStdT_Okr.value(0).toDouble(); else result = -1;

    db.close();
  }
  return result;
}

/*!
 * Metoda wykonująca zapytanie pobierające dane o temperaturze z wybranego zakresu dni.
 * W zależności od wybranej wartości w polu Pomiary::ZTemp. Wykonuje kilka zapytań do bazy danych i pobiera następujące informacje:
 * - Pomiary temperatury z zadanego okresu
 * - Maksymalna temperatura z zadanego okresu
 * - Minimalna temperatura z zadanego okresu
 * - Średnia temperatura z zadanego okresu
 * - Variancja temperatury z zadanego okresu
 * - Odchylenie standardowe temperatury z zadanego okresu
 * - Maksymalna odnotowana temperatura
 * - Minimalna odnotowana temperatura
 * - Pomiary temperatury z dzisiaj
 * - Maksymalna temperatura z dzisiaj
 * - Minimalna temperatura z dzisiaj
 *
 * \param[in] wPomiary - Wskaźnik na strukturę przechowjąca dane pomiarowe i rodzaj zapytania, które ma być sierowane do bazy danych.
 *
 * \retval 0  - Jeżeli połączenie z bazą danych zakończy się sukcesem.
 * \retval -1 - Jeżeli nie uda się uzyskać odpowiedzi od bazy danych.
 */
int BazaDanych::PobTemp_Data(StrPom* wPomiary){
    int result = 0;
    if(!db.open()){
      cout << db.lastError().text().toStdString() << endl;
      return -1;
    }
    else{
      QString Zapytanie = "BMP280TEMP";

      QSqlQuery QueryPomiary_Okr;
      QString SklZapPomiary_Okr;

      QSqlQuery QueryPomiary_Dzien;
      QString SklZapPomiary_Dzien;

      QSqlQuery QueryMaxT_Okr;
      QString SklZapMaxT_Okr;

      QSqlQuery QueryMaxT_Dzien;
      QString SklZapMaxT_Dzien;

      QSqlQuery QueryMinT_Okr;
      QString SklZapMinT_Okr;

      QSqlQuery QueryMinT_Dzien;
      QString SklZapMinT_Dzien;

      QSqlQuery QueryMaxT;
      QString SklZapMaxT;

      QSqlQuery QueryMinT;
      QString SklZapMinT;

      QSqlQuery QueryAvgT_Okr;
      QString SklZapAvgT_Okr;

      QSqlQuery QueryVarT_Okr;
      QString SklZapVarT_Okr;

      QSqlQuery QueryStdT_Okr;
      QString SklZapStdT_Okr;


      SklZapPomiary_Okr = "CALL pPomiary_Date(" + QString::number(_CzujnikID) +", '"+ Zapytanie +"', '" + wPomiary->DataOD.toString("yyyy-MM-dd") + "' , '00:00:00', '"
                          + wPomiary->DataDO.toString("yyyy-MM-dd") + "', '24:00:00');";
      cout << SklZapPomiary_Okr.toStdString() << endl;
      QueryPomiary_Okr.exec(SklZapPomiary_Okr);

      SklZapPomiary_Dzien = "CALL pPomiary(" + QString::number(_CzujnikID) +", '"+ Zapytanie +"', 1);";
      cout << SklZapPomiary_Dzien.toStdString() << endl;
      QueryPomiary_Dzien.exec(SklZapPomiary_Dzien);

      SklZapMaxT_Okr = "CALL pMAX_Date(" + QString::number(_CzujnikID) +", '"+ Zapytanie +"', '" + wPomiary->DataOD.toString("yyyy-MM-dd") + "' , '00:00:00', '"
              + wPomiary->DataDO.toString("yyyy-MM-dd") + "', '24:00:00');";
      cout << SklZapMaxT_Okr.toStdString() << endl;
      QueryMaxT_Okr.exec(SklZapMaxT_Okr);

      SklZapMaxT_Dzien = "SELECT fMAX_DaysEarlier(" + QString::number(_CzujnikID) +", '"+ Zapytanie +"', 1);";
      cout << SklZapMaxT_Dzien.toStdString() << endl;
      QueryMaxT_Dzien.exec(SklZapMaxT_Dzien);

      SklZapMinT_Okr = "CALL pMIN_Date(" + QString::number(_CzujnikID) +", '"+ Zapytanie +"', '" + wPomiary->DataOD.toString("yyyy-MM-dd") + "' , '00:00:00', '"
              + wPomiary->DataDO.toString("yyyy-MM-dd") + "', '24:00:00');";
      cout << SklZapMinT_Okr.toStdString() << endl;
      QueryMinT_Okr.exec(SklZapMinT_Okr);

      SklZapMinT_Dzien = "SELECT fMIN_DaysEarlier(" + QString::number(_CzujnikID) +", '"+ Zapytanie +"', 1);";
      cout << SklZapMinT_Dzien.toStdString() << endl;
      QueryMinT_Dzien.exec(SklZapMinT_Dzien);

      SklZapMaxT = "SELECT fMAX(" + QString::number(_CzujnikID) +", '"+ Zapytanie +"');";
      cout << SklZapMaxT.toStdString() << endl;
      QueryMaxT.exec(SklZapMaxT);

      SklZapMinT = "SELECT fMIN(" + QString::number(_CzujnikID) +", '"+ Zapytanie +"');";
      cout << SklZapMinT.toStdString() << endl;
      QueryMinT.exec(SklZapMinT);

      SklZapAvgT_Okr = "CALL pAVG_Date(" + QString::number(_CzujnikID) +", '"+ Zapytanie +"', '" + wPomiary->DataOD.toString("yyyy-MM-dd") + "' , '00:00:00', '"
              + wPomiary->DataDO.toString("yyyy-MM-dd") + "', '24:00:00');";
      cout << SklZapAvgT_Okr.toStdString() << endl;
      QueryAvgT_Okr.exec(SklZapAvgT_Okr);

      SklZapVarT_Okr = "CALL pVAR_Date(" + QString::number(_CzujnikID) +", '"+ Zapytanie +"', '" + wPomiary->DataOD.toString("yyyy-MM-dd") + "' , '00:00:00', '"
              + wPomiary->DataDO.toString("yyyy-MM-dd") + "', '24:00:00');";
      cout << SklZapVarT_Okr.toStdString() << endl;
      QueryVarT_Okr.exec(SklZapVarT_Okr);

      SklZapStdT_Okr = "CALL pSTD_Date(" + QString::number(_CzujnikID) +", '"+ Zapytanie +"', '" + wPomiary->DataOD.toString("yyyy-MM-dd") + "' , '00:00:00', '"
              + wPomiary->DataDO.toString("yyyy-MM-dd") + "', '24:00:00');";
      cout << SklZapStdT_Okr.toStdString() << endl;
      QueryStdT_Okr.exec(SklZapStdT_Okr);


      int i = 0;
      if(QueryPomiary_Okr.size() != -1){
          wPomiary->TempOkr.ReallocateTab(QueryPomiary_Okr.size());
          while(QueryPomiary_Okr.next()){
            wPomiary->TempOkr.Tab(i).Czas = QueryPomiary_Okr.value(1).toTime();   //cout << Pomiary->TempOkr.Tab(i).Czas.toString("hh:mm:ss").toStdString() << endl;
            wPomiary->TempOkr.Tab(i).Data = QueryPomiary_Okr.value(2).toDate();   //cout << Pomiary->TempOkr.Tab(i).Data.toString("yyyy-MM-dd").toStdString() << endl;
            wPomiary->TempOkr.Tab(i).Wartosc = QueryPomiary_Okr.value(5).toDouble(); //cout << Pomiary->TempOkr.Tab(i).Wartosc << endl;
            ++i;
          }
      }
      else result = -1;
      i = 0;
      if(QueryPomiary_Dzien.size() != -1){
          wPomiary->TempDzis.ReallocateTab(QueryPomiary_Dzien.size());
          while(QueryPomiary_Dzien.next()){
            wPomiary->TempDzis.Tab(i).Czas = QueryPomiary_Dzien.value(1).toTime();   //cout << Pomiary->TempDzis.Tab(i).Czas.toString("hh:mm:ss").toStdString() << endl;
            wPomiary->TempDzis.Tab(i).Data = QueryPomiary_Dzien.value(2).toDate();   //cout << Pomiary->TempDzis.Tab(i).Data.toString("yyyy-MM-dd").toStdString() << endl;
            wPomiary->TempDzis.Tab(i).Wartosc = QueryPomiary_Dzien.value(5).toDouble(); //cout << Pomiary->TempDzis.Tab(i).Wartosc << endl;
            ++i;
          }
      }
      else result = -1;


      wPomiary->AktT = wPomiary->TempDzis.Tab(i-1).Wartosc; //cout << Pomiary->AktT << endl;
      if(QueryMaxT_Okr.first())   wPomiary->MaxTOkr  = QueryMaxT_Okr.value(0).toDouble();   else result = -1;
      if(QueryMaxT_Dzien.first()) wPomiary->MaxTDzis = QueryMaxT_Dzien.value(0).toDouble(); else result = -1;
      if(QueryMinT_Okr.first())   wPomiary->MinTOkr  = QueryMinT_Okr.value(0).toDouble();   else result = -1;
      if(QueryMinT_Dzien.first()) wPomiary->MinTDzis = QueryMinT_Dzien.value(0).toDouble(); else result = -1;
      if(QueryMaxT.first()) wPomiary->MaxT = QueryMaxT.value(0).toDouble(); else result = -1;
      if(QueryMinT.first()) wPomiary->MinT = QueryMinT.value(0).toDouble(); else result = -1;
      if(QueryAvgT_Okr.first()) wPomiary->AvgTOkr = QueryAvgT_Okr.value(0).toDouble(); else result = -1;
      if(QueryVarT_Okr.first()) wPomiary->VarTOkr = QueryVarT_Okr.value(0).toDouble(); else result = -1;
      if(QueryStdT_Okr.first()) wPomiary->StdTOkr = QueryStdT_Okr.value(0).toDouble(); else result = -1;

      db.close();
    }
    return result;
}

int BazaDanych::PobWilg_Data(StrPom *wPomiary){
    int result = 0;
    if(!db.open()){
      cout << db.lastError().text().toStdString() << endl;
      return -1;
    }
    else{
      QString Zapytanie = "DHT11WILG";

      QSqlQuery QueryPomiary_Okr;
      QString SklZapPomiary_Okr;

      QSqlQuery QueryPomiary_Dzien;
      QString SklZapPomiary_Dzien;

      QSqlQuery QueryMaxW_Okr;
      QString SklZapMaxW_Okr;

      QSqlQuery QueryMaxW_Dzien;
      QString SklZapMaxW_Dzien;

      QSqlQuery QueryMinW_Okr;
      QString SklZapMinW_Okr;

      QSqlQuery QueryMinW_Dzien;
      QString SklZapMinW_Dzien;

      QSqlQuery QueryMaxW;
      QString SklZapMaxW;

      QSqlQuery QueryMinW;
      QString SklZapMinW;

      QSqlQuery QueryAvgW_Okr;
      QString SklZapAvgW_Okr;

      QSqlQuery QueryVarW_Okr;
      QString SklZapVarW_Okr;

      QSqlQuery QueryStdW_Okr;
      QString SklZapStdW_Okr;


      SklZapPomiary_Okr = "CALL pPomiary_Date(" + QString::number(_CzujnikID) +", '"+ Zapytanie +"', '" + wPomiary->DataOD.toString("yyyy-MM-dd") + "' , '00:00:00', '"
                          + wPomiary->DataDO.toString("yyyy-MM-dd") + "', '24:00:00');";
      cout << SklZapPomiary_Okr.toStdString() << endl;
      QueryPomiary_Okr.exec(SklZapPomiary_Okr);

      SklZapPomiary_Dzien = "CALL pPomiary(" + QString::number(_CzujnikID) +", '"+ Zapytanie +"', 1);";
      cout << SklZapPomiary_Dzien.toStdString() << endl;
      QueryPomiary_Dzien.exec(SklZapPomiary_Dzien);

      SklZapMaxW_Okr = "CALL pMAX_Date(" + QString::number(_CzujnikID) +", '"+ Zapytanie +"', '" + wPomiary->DataOD.toString("yyyy-MM-dd") + "' , '00:00:00', '"
              + wPomiary->DataDO.toString("yyyy-MM-dd") + "', '24:00:00');";
      cout << SklZapMaxW_Okr.toStdString() << endl;
      QueryMaxW_Okr.exec(SklZapMaxW_Okr);

      SklZapMaxW_Dzien = "SELECT fMAX_DaysEarlier(" + QString::number(_CzujnikID) +", '"+ Zapytanie +"', 1);";
      cout << SklZapMaxW_Dzien.toStdString() << endl;
      QueryMaxW_Dzien.exec(SklZapMaxW_Dzien);

      SklZapMinW_Okr = "CALL pMIN_Date(" + QString::number(_CzujnikID) +", '"+ Zapytanie +"', '" + wPomiary->DataOD.toString("yyyy-MM-dd") + "' , '00:00:00', '"
              + wPomiary->DataDO.toString("yyyy-MM-dd") + "', '24:00:00');";
      cout << SklZapMinW_Okr.toStdString() << endl;
      QueryMinW_Okr.exec(SklZapMinW_Okr);

      SklZapMinW_Dzien = "SELECT fMIN_DaysEarlier(" + QString::number(_CzujnikID) +", '"+ Zapytanie +"', 1);";
      cout << SklZapMinW_Dzien.toStdString() << endl;
      QueryMinW_Dzien.exec(SklZapMinW_Dzien);

      SklZapMaxW = "SELECT fMAX(" + QString::number(_CzujnikID) +", '"+ Zapytanie +"');";
      cout << SklZapMaxW.toStdString() << endl;
      QueryMaxW.exec(SklZapMaxW);

      SklZapMinW = "SELECT fMIN(" + QString::number(_CzujnikID) +", '"+ Zapytanie +"');";
      cout << SklZapMinW.toStdString() << endl;
      QueryMinW.exec(SklZapMinW);

      SklZapAvgW_Okr = "CALL pAVG_Date(" + QString::number(_CzujnikID) +", '"+ Zapytanie +"', '" + wPomiary->DataOD.toString("yyyy-MM-dd") + "' , '00:00:00', '"
              + wPomiary->DataDO.toString("yyyy-MM-dd") + "', '24:00:00');";
      cout << SklZapAvgW_Okr.toStdString() << endl;
      QueryAvgW_Okr.exec(SklZapAvgW_Okr);

      SklZapVarW_Okr = "CALL pVAR_Date(" + QString::number(_CzujnikID) +", '"+ Zapytanie +"', '" + wPomiary->DataOD.toString("yyyy-MM-dd") + "' , '00:00:00', '"
              + wPomiary->DataDO.toString("yyyy-MM-dd") + "', '24:00:00');";
      cout << SklZapVarW_Okr.toStdString() << endl;
      QueryVarW_Okr.exec(SklZapVarW_Okr);

      SklZapStdW_Okr = "CALL pSTD_Date(" + QString::number(_CzujnikID) +", '"+ Zapytanie +"', '" + wPomiary->DataOD.toString("yyyy-MM-dd") + "' , '00:00:00', '"
              + wPomiary->DataDO.toString("yyyy-MM-dd") + "', '24:00:00');";
      cout << SklZapStdW_Okr.toStdString() << endl;
      QueryStdW_Okr.exec(SklZapStdW_Okr);


      int i = 0;
      if(QueryPomiary_Okr.size() != -1){
          wPomiary->WilgOkr.ReallocateTab(QueryPomiary_Okr.size());
          while(QueryPomiary_Okr.next()){
            wPomiary->WilgOkr.Tab(i).Czas = QueryPomiary_Okr.value(1).toTime();   //cout << Pomiary->TempOkr.Tab(i).Czas.toString("hh:mm:ss").toStdString() << endl;
            wPomiary->WilgOkr.Tab(i).Data = QueryPomiary_Okr.value(2).toDate();   //cout << Pomiary->TempOkr.Tab(i).Data.toString("yyyy-MM-dd").toStdString() << endl;
            wPomiary->WilgOkr.Tab(i).Wartosc = QueryPomiary_Okr.value(5).toDouble(); //cout << Pomiary->TempOkr.Tab(i).Wartosc << endl;
            ++i;
          }
      }
      else result = -1;
      i = 0;
      if(QueryPomiary_Dzien.size() != -1){
          wPomiary->WilgDzis.ReallocateTab(QueryPomiary_Dzien.size());
          while(QueryPomiary_Dzien.next()){
            wPomiary->WilgDzis.Tab(i).Czas = QueryPomiary_Dzien.value(1).toTime();   //cout << Pomiary->TempDzis.Tab(i).Czas.toString("hh:mm:ss").toStdString() << endl;
            wPomiary->WilgDzis.Tab(i).Data = QueryPomiary_Dzien.value(2).toDate();   //cout << Pomiary->TempDzis.Tab(i).Data.toString("yyyy-MM-dd").toStdString() << endl;
            wPomiary->WilgDzis.Tab(i).Wartosc = QueryPomiary_Dzien.value(5).toDouble(); //cout << Pomiary->TempDzis.Tab(i).Wartosc << endl;
            ++i;
          }
      }
      else result = -1;


      wPomiary->AktW = wPomiary->WilgDzis.Tab(i-1).Wartosc; //cout << Pomiary->AktT << endl;
      if(QueryMaxW_Okr.first())   wPomiary->MaxTOkr  = QueryMaxW_Okr.value(0).toDouble();   else result = -1;
      if(QueryMaxW_Dzien.first()) wPomiary->MaxTDzis = QueryMaxW_Dzien.value(0).toDouble(); else result = -1;
      if(QueryMinW_Okr.first())   wPomiary->MinTOkr  = QueryMinW_Okr.value(0).toDouble();   else result = -1;
      if(QueryMinW_Dzien.first()) wPomiary->MinTDzis = QueryMinW_Dzien.value(0).toDouble(); else result = -1;
      if(QueryMaxW.first()) wPomiary->MaxT = QueryMaxW.value(0).toDouble(); else result = -1;
      if(QueryMinW.first()) wPomiary->MinT = QueryMinW.value(0).toDouble(); else result = -1;
      if(QueryAvgW_Okr.first()) wPomiary->AvgTOkr = QueryAvgW_Okr.value(0).toDouble(); else result = -1;
      if(QueryVarW_Okr.first()) wPomiary->VarTOkr = QueryVarW_Okr.value(0).toDouble(); else result = -1;
      if(QueryStdW_Okr.first()) wPomiary->StdTOkr = QueryStdW_Okr.value(0).toDouble(); else result = -1;

      db.close();
    }
    return result;
}

int BazaDanych::PobCisn_Data(StrPom* wPomiary){
    cout << "Pob cis" << endl;
    int result = 0;
    if(!db.open()){
      cout << db.lastError().text().toStdString() << endl;
      return -1;
    }
    else{
      QString Zapytanie = "BMP280CISN";

      QSqlQuery QueryPomiary_Okr;
      QString SklZapPomiary_Okr;

      QSqlQuery QueryPomiary_Dzien;
      QString SklZapPomiary_Dzien;

      QSqlQuery QueryMaxC_Okr;
      QString SklZapMaxC_Okr;

      QSqlQuery QueryMaxC_Dzien;
      QString SklZapMaxC_Dzien;

      QSqlQuery QueryMinC_Okr;
      QString SklZapMinC_Okr;

      QSqlQuery QueryMinC_Dzien;
      QString SklZapMinC_Dzien;

      QSqlQuery QueryMaxC;
      QString SklZapMaxC;

      QSqlQuery QueryMinC;
      QString SklZapMinC;

      QSqlQuery QueryAvgC_Okr;
      QString SklZapAvgC_Okr;

      QSqlQuery QueryVarC_Okr;
      QString SklZapVarC_Okr;

      QSqlQuery QueryStdC_Okr;
      QString SklZapStdC_Okr;


      SklZapPomiary_Okr = "CALL pPomiary_Date(" + QString::number(_CzujnikID) +", '"+ Zapytanie +"', '" + wPomiary->DataOD.toString("yyyy-MM-dd") + "' , '00:00:00', '"
                          + wPomiary->DataDO.toString("yyyy-MM-dd") + "', '24:00:00');";
      cout << SklZapPomiary_Okr.toStdString() << endl;
      QueryPomiary_Okr.exec(SklZapPomiary_Okr);

      SklZapPomiary_Dzien = "CALL pPomiary(" + QString::number(_CzujnikID) +", '"+ Zapytanie +"', 1);";
      cout << SklZapPomiary_Dzien.toStdString() << endl;
      QueryPomiary_Dzien.exec(SklZapPomiary_Dzien);

      SklZapMaxC_Okr = "CALL pMAX_Date(" + QString::number(_CzujnikID) +", '"+ Zapytanie +"', '" + wPomiary->DataOD.toString("yyyy-MM-dd") + "' , '00:00:00', '"
              + wPomiary->DataDO.toString("yyyy-MM-dd") + "', '24:00:00');";
      cout << SklZapMaxC_Okr.toStdString() << endl;
      QueryMaxC_Okr.exec(SklZapMaxC_Okr);

      SklZapMaxC_Dzien = "SELECT fMAX_DaysEarlier(" + QString::number(_CzujnikID) +", '"+ Zapytanie +"', 1);";
      cout << SklZapMaxC_Dzien.toStdString() << endl;
      QueryMaxC_Dzien.exec(SklZapMaxC_Dzien);

      SklZapMinC_Okr = "CALL pMIN_Date(" + QString::number(_CzujnikID) +", '"+ Zapytanie +"', '" + wPomiary->DataOD.toString("yyyy-MM-dd") + "' , '00:00:00', '"
              + wPomiary->DataDO.toString("yyyy-MM-dd") + "', '24:00:00');";
       cout << SklZapMinC_Okr.toStdString() << endl;
      QueryMinC_Okr.exec(SklZapMinC_Okr);

      SklZapMinC_Dzien = "SELECT fMIN_DaysEarlier(" + QString::number(_CzujnikID) +", '"+ Zapytanie +"', 1);";
      cout << SklZapMinC_Dzien.toStdString() << endl;
      QueryMinC_Dzien.exec(SklZapMinC_Dzien);

      SklZapMaxC = "SELECT fMAX(" + QString::number(_CzujnikID) +", '"+ Zapytanie +"');";
      cout << SklZapMaxC.toStdString() << endl;
      QueryMaxC.exec(SklZapMaxC);

      SklZapMinC = "SELECT fMIN(" + QString::number(_CzujnikID) +", '"+ Zapytanie +"');";
      cout << SklZapMinC.toStdString() << endl;
      QueryMinC.exec(SklZapMinC);

      SklZapAvgC_Okr = "CALL pAVG_Date(" + QString::number(_CzujnikID) +", '"+ Zapytanie +"', '" + wPomiary->DataOD.toString("yyyy-MM-dd") + "' , '00:00:00', '"
              + wPomiary->DataDO.toString("yyyy-MM-dd") + "', '24:00:00');";
       cout << SklZapAvgC_Okr.toStdString() << endl;
      QueryAvgC_Okr.exec(SklZapAvgC_Okr);

      SklZapVarC_Okr = "CALL pVAR_Date(" + QString::number(_CzujnikID) +", '"+ Zapytanie +"', '" + wPomiary->DataOD.toString("yyyy-MM-dd") + "' , '00:00:00', '"
              + wPomiary->DataDO.toString("yyyy-MM-dd") + "', '24:00:00');";
       cout << SklZapVarC_Okr.toStdString() << endl;
      QueryVarC_Okr.exec(SklZapVarC_Okr);

      SklZapStdC_Okr = "CALL pSTD_Date(" + QString::number(_CzujnikID) +", '"+ Zapytanie +"', '" + wPomiary->DataOD.toString("yyyy-MM-dd") + "' , '00:00:00', '"
              + wPomiary->DataDO.toString("yyyy-MM-dd") + "', '24:00:00');";
       cout << SklZapStdC_Okr.toStdString() << endl;
      QueryStdC_Okr.exec(SklZapStdC_Okr);


      int i = 0;
      if(QueryPomiary_Okr.size() != -1){
          wPomiary->CisnOkr.ReallocateTab(QueryPomiary_Okr.size());
          while(QueryPomiary_Okr.next()){
            wPomiary->CisnOkr.Tab(i).Czas = QueryPomiary_Okr.value(1).toTime();   //cout << Pomiary->TempOkr.Tab(i).Czas.toString("hh:mm:ss").toStdString() << endl;
            wPomiary->CisnOkr.Tab(i).Data = QueryPomiary_Okr.value(2).toDate();   //cout << Pomiary->TempOkr.Tab(i).Data.toString("yyyy-MM-dd").toStdString() << endl;
            wPomiary->CisnOkr.Tab(i).Wartosc = QueryPomiary_Okr.value(5).toDouble(); //cout << Pomiary->TempOkr.Tab(i).Wartosc << endl;
            ++i;
          }
      }
      else result = -1;
      i = 0;
      if(QueryPomiary_Dzien.size() != -1){
          wPomiary->CisnDzis.ReallocateTab(QueryPomiary_Dzien.size());
          while(QueryPomiary_Dzien.next()){
            wPomiary->CisnDzis.Tab(i).Czas = QueryPomiary_Dzien.value(1).toTime();   //cout << Pomiary->TempDzis.Tab(i).Czas.toString("hh:mm:ss").toStdString() << endl;
            wPomiary->CisnDzis.Tab(i).Data = QueryPomiary_Dzien.value(2).toDate();   //cout << Pomiary->TempDzis.Tab(i).Data.toString("yyyy-MM-dd").toStdString() << endl;
            wPomiary->CisnDzis.Tab(i).Wartosc = QueryPomiary_Dzien.value(5).toDouble(); //cout << Pomiary->TempDzis.Tab(i).Wartosc << endl;
            ++i;
          }
      }
      else result = -1;


      wPomiary->AktC = wPomiary->CisnDzis.Tab(i-1).Wartosc; //cout << Pomiary->AktT << endl;
      if(QueryMaxC_Okr.first())   wPomiary->MaxCOkr  = QueryMaxC_Okr.value(0).toDouble();   else result = -1;
      if(QueryMaxC_Dzien.first()) wPomiary->MaxCDzis = QueryMaxC_Dzien.value(0).toDouble(); else result = -1;
      if(QueryMinC_Okr.first())   wPomiary->MinCOkr  = QueryMinC_Okr.value(0).toDouble();   else result = -1;
      if(QueryMinC_Dzien.first()) wPomiary->MinCDzis = QueryMinC_Dzien.value(0).toDouble(); else result = -1;
      if(QueryMaxC.first()) wPomiary->MaxC = QueryMaxC.value(0).toDouble(); else result = -1;
      if(QueryMinC.first()) wPomiary->MinC = QueryMinC.value(0).toDouble(); else result = -1;
      if(QueryAvgC_Okr.first()) wPomiary->AvgCOkr = QueryAvgC_Okr.value(0).toDouble(); else result = -1;
      if(QueryVarC_Okr.first()) wPomiary->VarCOkr = QueryVarC_Okr.value(0).toDouble(); else result = -1;
      if(QueryStdC_Okr.first()) wPomiary->StdCOkr = QueryStdC_Okr.value(0).toDouble(); else result = -1;

      db.close();
    }
    return result;
}


/*!
 * Metoda wykonująca zapytanie pobierające dane o ciśnieniu n dni wstecz.
 * W zależności od wybranej wartości w polu Pomiary::ZCisn. Wykonuje kilka zapytań do bazy danych i pobiera następujące informacje:
 * - Pomiary ciśnienia z zadanego okresu
 * - Maksymalne ciśnienie z zadanego okresu
 * - Minimalne ciśnienie z zadanego okresu
 * - Średnie ciśnienie z zadanego okresu
 * - Variancja ciśnienia z zadanego okresu
 * - Odchylenie standardowe ciśnienia z zadanego okresu
 * - Maksymalne odnotowane ciśnienie
 * - Minimalne odnotowane ciśnienie
 * - Pomiary ciśnienia z dzisiaj
 * - Maksymalne ciśnienie z dzisiaj
 * - Minimalne ciśnienie z dzisiaj
 *
 * \param[in] wPomiary - Wskaźnik na strukturę przechowjąca dane pomiarowe i rodzaj zapytania, które ma być sierowane do bazy danych.
 *
 * \retval 0  - Jeżeli połączenie z bazą danych zakończy się sukcesem.
 * \retval -1 - Jeżeli nie uda się uzyskać odpowiedzi od bazy danych.
 */
int BazaDanych::PobCisn_Dni(StrPom* wPomiary){
    int result = 0;
    QString Zapytanie = "BMP280CISN";
    if(!db.open()){
      cout << db.lastError().text().toStdString() << endl;
      return -1;
    }
    else{
      QSqlQuery QueryPomiary_Okr;
      QString SklZapPomiary_Okr;

      QSqlQuery QueryPomiary_Dzien;
      QString SklZapPomiary_Dzien;

      QSqlQuery QueryMaxC_Okr;
      QString SklZapMaxC_Okr;

      QSqlQuery QueryMaxC_Dzien;
      QString SklZapMaxC_Dzien;

      QSqlQuery QueryMinC_Okr;
      QString SklZapMinC_Okr;

      QSqlQuery QueryMinC_Dzien;
      QString SklZapMinC_Dzien;

      QSqlQuery QueryMaxC;
      QString SklZapMaxC;

      QSqlQuery QueryMinC;
      QString SklZapMinC;

      QSqlQuery QueryAvgC_Okr;
      QString SklZapAvgC_Okr;

      QSqlQuery QueryVarC_Okr;
      QString SklZapVarC_Okr;

      QSqlQuery QueryStdC_Okr;
      QString SklZapStdC_Okr;

      int Dni = 1;
      switch(wPomiary->ZCisn){
        case Dzisiaj:
          Dni = 1;
          break;
        case Tydzien:
          Dni = 7;
          break;
        case Miesiac:
          Dni = 30;
          break;
        default:
          db.close();
          cerr << "Zle podany zakres dni do wyswietlenia w BazaDanych::PobCisn_Dni()" << endl;
          return -1;
      }

      SklZapPomiary_Okr = "CALL pPomiary(" + QString::number(_CzujnikID) +", '"+ Zapytanie +"', " + QString::number(Dni) + ");";
      cout << SklZapPomiary_Okr.toStdString() << endl;
      QueryPomiary_Okr.exec(SklZapPomiary_Okr);

      SklZapPomiary_Dzien = "CALL pPomiary(" + QString::number(_CzujnikID) +", '"+ Zapytanie +"', 1);";
      cout << SklZapPomiary_Dzien.toStdString() << endl;
      QueryPomiary_Dzien.exec(SklZapPomiary_Dzien);

      SklZapMaxC_Okr = "SELECT fMAX_DaysEarlier(" + QString::number(_CzujnikID) +", '"+ Zapytanie +"', " + QString::number(Dni) + ");";
      cout << SklZapMaxC_Okr.toStdString() << endl;
      QueryMaxC_Okr.exec(SklZapMaxC_Okr);

      SklZapMaxC_Dzien = "SELECT fMAX_DaysEarlier(" + QString::number(_CzujnikID) +", '"+ Zapytanie +"', 1);";
      cout << SklZapMaxC_Dzien.toStdString() << endl;
      QueryMaxC_Dzien.exec(SklZapMaxC_Dzien);

      SklZapMinC_Okr = "SELECT fMIN_DaysEarlier(" + QString::number(_CzujnikID) +", '"+ Zapytanie +"', " + QString::number(Dni) + ");";
      cout << SklZapMinC_Okr.toStdString() << endl;
      QueryMinC_Okr.exec(SklZapMinC_Okr);

      SklZapMinC_Dzien = "SELECT fMIN_DaysEarlier(" + QString::number(_CzujnikID) +", '"+ Zapytanie +"', 1);";
      cout << SklZapMinC_Dzien.toStdString() << endl;
      QueryMinC_Dzien.exec(SklZapMinC_Dzien);

      SklZapMaxC = "SELECT fMAX(" + QString::number(_CzujnikID) +", '"+ Zapytanie +"');";
      cout << SklZapMaxC.toStdString() << endl;
      QueryMaxC.exec(SklZapMaxC);

      SklZapMinC = "SELECT fMIN(" + QString::number(_CzujnikID) +", '"+ Zapytanie +"');";
      cout << SklZapMinC.toStdString() << endl;
      QueryMinC.exec(SklZapMinC);

      SklZapAvgC_Okr = "SELECT fAVG_DaysEarlier(" + QString::number(_CzujnikID) +", '"+ Zapytanie +"', " + QString::number(Dni) + ");";
      cout << SklZapAvgC_Okr.toStdString() << endl;
      QueryAvgC_Okr.exec(SklZapAvgC_Okr);

      SklZapVarC_Okr = "SELECT fVAR_DaysEarlier(" + QString::number(_CzujnikID) +", '"+ Zapytanie +"', " + QString::number(Dni) + ");";
      cout << SklZapVarC_Okr.toStdString() << endl;
      QueryVarC_Okr.exec(SklZapVarC_Okr);

      SklZapStdC_Okr = "SELECT fSTD_DaysEarlier(" + QString::number(_CzujnikID) +", '"+ Zapytanie +"', " + QString::number(Dni) + ");";
      cout << SklZapStdC_Okr.toStdString() << endl;
      QueryStdC_Okr.exec(SklZapStdC_Okr);

      int i = 0;
      if(QueryPomiary_Okr.size() != -1){
          wPomiary->CisnOkr.ReallocateTab(QueryPomiary_Okr.size());
          while(QueryPomiary_Okr.next()){
            wPomiary->CisnOkr.Tab(i).Czas = QueryPomiary_Okr.value(1).toTime();   //cout << Pomiary->CisnOkr.Tab(i).Czas.toString("hh:mm:ss").toStdString() << endl;
            wPomiary->CisnOkr.Tab(i).Data = QueryPomiary_Okr.value(2).toDate();   //cout << Pomiary->CisnOkr.Tab(i).Data.toString("yyyy-MM-dd").toStdString() << endl;
            wPomiary->CisnOkr.Tab(i).Wartosc = QueryPomiary_Okr.value(5).toDouble(); //cout << Pomiary->CisnOkr.Tab(i).Wartosc << endl;
            ++i;
          }
      }
      else result = -1;
      i = 0;
      if(QueryPomiary_Dzien.size() != -1){
          wPomiary->CisnDzis.ReallocateTab(QueryPomiary_Dzien.size());
          while(QueryPomiary_Dzien.next()){
            wPomiary->CisnDzis.Tab(i).Czas = QueryPomiary_Dzien.value(1).toTime();   //cout << Pomiary->CisnDzis.Tab(i).Czas.toString("hh:mm:ss").toStdString() << endl;
            wPomiary->CisnDzis.Tab(i).Data = QueryPomiary_Dzien.value(2).toDate();   //cout << Pomiary->CisnDzis.Tab(i).Data.toString("yyyy-MM-dd").toStdString() << endl;
            wPomiary->CisnDzis.Tab(i).Wartosc = QueryPomiary_Dzien.value(5).toDouble(); //cout << Pomiary->CisnDzis.Tab(i).Wartosc << endl;
            ++i;
          }
      }
      else result = -1;


      wPomiary->AktC = wPomiary->CisnDzis.Tab(i-1).Wartosc; //cout << Pomiary->AktT << endl;
      if(QueryMaxC_Okr.first())   wPomiary->MaxCOkr  = QueryMaxC_Okr.value(0).toDouble();   else result = -1;
      if(QueryMaxC_Dzien.first()) wPomiary->MaxCDzis = QueryMaxC_Dzien.value(0).toDouble(); else result = -1;
      if(QueryMinC_Okr.first())   wPomiary->MinCOkr  = QueryMinC_Okr.value(0).toDouble();   else result = -1;
      if(QueryMinC_Dzien.first()) wPomiary->MinCDzis = QueryMinC_Dzien.value(0).toDouble(); else result = -1;
      if(QueryMaxC.first()) wPomiary->MaxC = QueryMaxC.value(0).toDouble(); else result = -1;
      if(QueryMinC.first()) wPomiary->MinC = QueryMinC.value(0).toDouble(); else result = -1;
      if(QueryAvgC_Okr.first()) wPomiary->AvgCOkr = QueryAvgC_Okr.value(0).toDouble(); else result = -1;
      if(QueryVarC_Okr.first()) wPomiary->VarCOkr = QueryVarC_Okr.value(0).toDouble(); else result = -1;
      if(QueryStdC_Okr.first()) wPomiary->StdCOkr = QueryStdC_Okr.value(0).toDouble(); else result = -1;

      db.close();
    }
    return result;
}

BazaDanych::~BazaDanych(){
}

