#include "PomiaryTablica.hpp"

#include <iostream>

using namespace std;

PomiaryTablica::PomiaryTablica(){
  _tab = nullptr;
  _size = 0;
}

PomiaryTablica::~PomiaryTablica(){
  if(_tab != nullptr) delete[] _tab;
}

int PomiaryTablica::GetSize() const{
  return _size;
}

void PomiaryTablica::ReallocateTab(int size){
  if(_tab != nullptr){
    delete[] _tab;
  }
  if(size == 0){
    _tab = nullptr;
    _size = 0;
  }
  _tab = new WpisPomiarowy[size];
  _size = size;
}

WpisPomiarowy& PomiaryTablica::Tab(int w){
  if(w <= _size){
    return _tab[w];
  }
  else{
    cerr << "Zly wymiar wprowadzony w Pomiary::Tab" << endl;
    return _tab[0];
  }
  return _tab[0];
}
