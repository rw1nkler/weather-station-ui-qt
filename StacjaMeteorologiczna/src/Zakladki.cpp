#include <QWidget>
#include <QApplication>
#include <QTabWidget>
#include <iostream>

#include "Zakladki.hpp"
#include "ZakladkaOgolne.hpp"
#include "ZakladkaTemp.hpp"
#include "ZakladkaCisn.hpp"
#include "ZakladkaWilg.hpp"
#include "StrPom.hpp"
#include "WatekBazyDanych.hpp"
#include "OknoGlowne.hpp"

using namespace std;

Zakladki::Zakladki(QWidget *wRodzic): QTabWidget(wRodzic){
  addTab(_wZakladkaOgolne = new ZakladkaOgolne(this), "Ogólne");
  addTab(_wZakladkaTemp = new ZakladkaTemp(this), "Temperatura");
  addTab(_wZakladkaWilg = new ZakladkaWilg(this), "Wilgotność");
  addTab(_wZakladkaCisn = new ZakladkaCisn(this), "Ciśnienie");
}


void Zakladki::GdyPobranoDane(){
    cout << "Pobrano dane" << endl;
    OknoGlowne::GetwMutex()->lock();
    _wZakladkaOgolne->uaktualnij();
    _wZakladkaWilg->uaktualnij();
    _wZakladkaTemp->uaktualnij();
    _wZakladkaCisn->uaktualnij();
    OknoGlowne::GetwMutex()->unlock();
}
