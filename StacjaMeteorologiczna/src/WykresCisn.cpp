#include "WykresCisn.hpp"
#include "PomiaryTablica.hpp"
#include <climits>
#include <iostream>
using namespace std;

WykresCisn::WykresCisn(PomiaryTablica *wPomiary, IleDni Zakres){
  _wNazwa = new QString;
  _wPomiary = wPomiary;
  _Zakres = Zakres;

  _wWykres = new QChart();
  _wSeriaDanych = new QLineSeries(_wWykres);
  _wPredykcja = new QLineSeries(_wWykres);
  for(int i = 0; i < _wPomiary->GetSize(); ++i){
    QDateTime CzasWykres(_wPomiary->Tab(i).Data, _wPomiary->Tab(i).Czas);
    _wSeriaDanych->append(CzasWykres.toMSecsSinceEpoch(), _wPomiary->Tab(i).Wartosc);
  }
  _wWykres->addSeries(_wSeriaDanych);

  _osX = new QDateTimeAxis;
  _osX->setTickCount(10);

  if(_Zakres == Dzisiaj) _osX->setFormat("H:mm");
  else _osX->setFormat("d-MMM");

  _osX->setTitleText("Czas");
  _wWykres->addAxis(_osX,Qt::AlignBottom);
  _wSeriaDanych->attachAxis(_osX);

  _osY = new QValueAxis;
  _osY->setTickCount(6);
  _osY->setLabelFormat("%4.2f");
  _osY->setTitleText("Ciśnienie [hPa]");
  _wWykres->addAxis(_osY, Qt::AlignLeft);
  _wSeriaDanych->attachAxis(_osY);
}

WykresCisn::~WykresCisn(){
  delete _wNazwa;
}

void WykresCisn::setTitle(QString title){
  _wWykres->setTitle(title);
}

QString& WykresCisn::getNazwa() const{
  return *_wNazwa;
}

void WykresCisn::setNazwa(QString nazwa){
  *_wNazwa = nazwa;
}

QChart* WykresCisn::getWykres() const{
  return _wWykres;
}

void WykresCisn::uaktualnij(){
  double alpha = 0.3;
  _wSeriaDanych->clear();
  _wPredykcja->clear();
  QDateTime CzasMin(_wPomiary->Tab(0).Data, _wPomiary->Tab(0).Czas);
  QDateTime CzasMax(_wPomiary->Tab(_wPomiary->GetSize() - 1).Data, _wPomiary->Tab(_wPomiary->GetSize() - 1).Czas);
  double Max = 0;
  double Min = INT_MAX;

  QDateTime ypt(_wPomiary->Tab(0).Data, _wPomiary->Tab(0).Czas);
  double yp = _wPomiary->Tab(0).Wartosc;
  _wPredykcja->append(ypt.toMSecsSinceEpoch(), yp);
  for(int i = 0; i < _wPomiary->GetSize(); ++i){
     QDateTime CzasWykres(_wPomiary->Tab(i).Data, _wPomiary->Tab(i).Czas);
     QDateTime CzasPredykcja;
     if(i+1 < _wPomiary->GetSize()) {
         QDateTime CzasPredykcjatmp(_wPomiary->Tab(i+1).Data, _wPomiary->Tab(i+1).Czas);
         CzasPredykcja = CzasPredykcjatmp;
     }
     else{
         QDateTime CzasPredykcjatmp(_wPomiary->Tab(i).Data, _wPomiary->Tab(i).Czas);
         CzasPredykcjatmp.addSecs(15*60);
         CzasPredykcja = CzasPredykcjatmp;
     }
     if(_wPomiary->Tab(i).Wartosc < Min) Min = _wPomiary->Tab(i).Wartosc;
     if(_wPomiary->Tab(i).Wartosc > Max) Max = _wPomiary->Tab(i).Wartosc;
    _wSeriaDanych->append(CzasWykres.toMSecsSinceEpoch(), _wPomiary->Tab(i).Wartosc);
    double dpredykcja = yp + alpha*(_wPomiary->Tab(i).Wartosc - yp);
    yp = dpredykcja;
    _wPredykcja->append(CzasPredykcja.toMSecsSinceEpoch(), dpredykcja);
  }

  _osX->setRange(CzasMin, CzasMax);
  _osY->setRange(Min - 0.5, Max + 0.5);
  _wWykres->legend()->hide();
}

void WykresCisn::GdyChcePredykcje(int stan){
    switch(stan){
        case 0:
            _wWykres->removeSeries(_wPredykcja);
            break;
        case 2:
            _wWykres->addSeries(_wPredykcja);
            break;
    }
}
