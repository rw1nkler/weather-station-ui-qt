#include <QWidget>
#include <QGroupBox>
#include <QLabel>
#include <QVBoxLayout>
#include <QFormLayout>
#include <QGridLayout>
#include <QLineEdit>

#include "ZakladkaOgolne.hpp"
#include "Zakladki.hpp"
#include "OknoGlowne.hpp"

using namespace std;

ZakladkaOgolne::ZakladkaOgolne(QWidget *wRodzic): QWidget(wRodzic){
  QGridLayout *wOrganizerGrid = new QGridLayout;
  
  _wInfoGroupBox = new QGroupBox(this);
  _wIdCzujnika  = new QLabel(tr("ID czujnika: ") + "#DEAC98", _wInfoGroupBox);
  _wUlica       = new QLabel(tr("ul. ") + "Kamienna 3", _wInfoGroupBox);
  _wKodPocztowy = new QLabel("Kod Pocztowy: 73-513 Wrocław", _wInfoGroupBox);
  
  _wInfoGroupBox->setTitle(tr("Informacje o czujniku"));
  _wInfoGroupBox->adjustSize();

  QVBoxLayout *wOrganizerWer = new QVBoxLayout;
  wOrganizerWer->addWidget(_wIdCzujnika);
  wOrganizerWer->addWidget(_wUlica);
  wOrganizerWer->addWidget(_wKodPocztowy);
  wOrganizerWer->addStretch(1);
  _wInfoGroupBox->setLayout(wOrganizerWer);

  _wDaneGroupBox = new QGroupBox(this);
  QFormLayout *wOrganizerForm = new QFormLayout;
  _wEditMaksTemp  = new QLineEdit(_wDaneGroupBox);
  _wEditMaksTemp->setReadOnly(true);
  _wEditMaksTemp->setAlignment(Qt::AlignHCenter);
  _wEditMinTemp   = new QLineEdit(_wDaneGroupBox);
  _wEditMinTemp->setReadOnly(true);
  _wEditMinTemp->setAlignment(Qt::AlignHCenter);
  _wEditAktTemp   = new QLineEdit(_wDaneGroupBox);
  _wEditAktTemp->setReadOnly(true);
  _wEditAktTemp->setAlignment(Qt::AlignHCenter);
  _wEditAktWilg   = new QLineEdit(_wDaneGroupBox);
  _wEditAktWilg->setReadOnly(true);
  _wEditAktWilg->setAlignment(Qt::AlignHCenter);
  _wEditAktCisn   = new QLineEdit(_wDaneGroupBox);
  _wEditAktCisn->setReadOnly(true);
  _wEditAktCisn->setAlignment(Qt::AlignHCenter);

  
  wOrganizerForm->addRow(new QLabel(tr("Maksymalna temperatura w ciągu dnia:")), _wEditMaksTemp);
  wOrganizerForm->addRow(new QLabel(tr("Minimalna temperatura w ciągu dnia:")),  _wEditMinTemp);
  wOrganizerForm->addRow(new QLabel(tr("Aktualna temperatura:")), _wEditAktTemp);
  wOrganizerForm->addRow(new QLabel(tr("Aktualna wilgotność:")), _wEditAktWilg);
  wOrganizerForm->addRow(new QLabel(tr("Aktualne ciśnienie:")), _wEditAktCisn);
  _wDaneGroupBox->setLayout(wOrganizerForm);
  _wDaneGroupBox->setTitle(tr("Podstawowe dane"));
  _wDaneGroupBox->adjustSize();
  
  _wDaneGroupBox->setMaximumWidth(400);
  _wInfoGroupBox->setMaximumWidth(400);
  _wDaneGroupBox->setMinimumWidth(400);
  _wInfoGroupBox->setMinimumWidth(400);
  _wDaneGroupBox->adjustSize();
  _wInfoGroupBox->adjustSize();

  _wWykGroupBox = new QGroupBox(this);
  QVBoxLayout *wOrganizerWer2 = new QVBoxLayout;
  
  _wWykTemp = new WykresTemp(&(OknoGlowne::GetwPomiary()->TempDzis), Dzisiaj);
  _wWykTempWidok = new WykresWidok(_wWykTemp->getWykres(),_wWykGroupBox);
  QLabel* wWykTempNapis = new QLabel("Wykres Temperatury (Dzisiaj):", _wWykGroupBox);
  wWykTempNapis->adjustSize();
  wWykTempNapis->setMaximumHeight(wWykTempNapis->height());
  
  _wWykWilg = new WykresWilg(&(OknoGlowne::GetwPomiary()->WilgDzis), Dzisiaj);
  _wWykWilgWidok = new WykresWidok(_wWykWilg->getWykres(), _wWykGroupBox);
  QLabel* wWykWilgNapis = new QLabel("Wykres Wilgotności", _wWykGroupBox);
  wWykWilgNapis->adjustSize();
  wWykWilgNapis->setMaximumHeight(wWykTempNapis->height());
  _wWykCisn = new WykresCisn(&(OknoGlowne::GetwPomiary()->CisnDzis), Dzisiaj);
  _wWykCisnWidok = new WykresWidok(_wWykCisn->getWykres(), _wWykGroupBox);
  QLabel* wWykCisnNapis = new QLabel("Wykres Ciśnienia (Dzisiaj)", _wWykGroupBox);
  wWykCisnNapis->adjustSize();
  wWykCisnNapis->setMaximumHeight(wWykTempNapis->height());
  
  wOrganizerWer2->addWidget(wWykTempNapis);
  wOrganizerWer2->addWidget(_wWykTempWidok);
  wOrganizerWer2->addWidget(wWykWilgNapis);
  wOrganizerWer2->addWidget(_wWykWilgWidok);
  wOrganizerWer2->addWidget(wWykCisnNapis);
  wOrganizerWer2->addWidget(_wWykCisnWidok);
  _wWykGroupBox->setLayout(wOrganizerWer2);
  _wWykGroupBox->setTitle(tr("Wykresy pogodowe"));

  _wWykGroupBox->setMinimumWidth(900);
  _wWykGroupBox->setMinimumHeight(800);
  _wWykGroupBox->adjustSize();
  
  wOrganizerGrid->addWidget(_wInfoGroupBox, 0, 0, 2, 1);
  wOrganizerGrid->addWidget(_wDaneGroupBox, 2, 0, 4, 1);
  wOrganizerGrid->addWidget(_wWykGroupBox, 0, 1, 6, 5); 
  this->setLayout(wOrganizerGrid);

}

WykresTemp* ZakladkaOgolne::getWykresTemp(){
  return _wWykTemp;
}

WykresWilg* ZakladkaOgolne::getWykresWilg(){
  return _wWykWilg;
}

WykresCisn* ZakladkaOgolne::getWykresCisn(){
  return _wWykCisn;
}

void ZakladkaOgolne::uaktualnij(){
  _wWykTemp->uaktualnij();
  _wWykCisn->uaktualnij();
  _wWykWilg->uaktualnij();
  _wEditMaksTemp->setText(QString::number(OknoGlowne::GetwPomiary()->MaxT) + tr(" °C"));
  _wEditMinTemp->setText(QString::number(OknoGlowne::GetwPomiary()->MinT) + tr(" °C"));
  _wEditAktTemp->setText(QString::number(OknoGlowne::GetwPomiary()->AktT) + tr(" °C"));
  _wEditAktWilg->setText(QString::number(OknoGlowne::GetwPomiary()->AktW) + " %RH");
  _wEditAktCisn->setText(QString::number(OknoGlowne::GetwPomiary()->AktC) + " hPa");
}
