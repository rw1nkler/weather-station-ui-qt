#include <QWidget>
#include <QLabel>
#include <QGroupBox>
#include <QVBoxLayout>
#include <QGridLayout>
#include <QFormLayout>
#include <QComboBox>
#include <iostream>

#include "ZakladkaWilg.hpp"


#include <QWidget>
#include <QLabel>
#include <QGroupBox>
#include <QVBoxLayout>
#include <QGridLayout>
#include <QFormLayout>
#include <QComboBox>

#include "ZakladkaWilg.hpp"
#include "Zakladki.hpp"
#include "WykresWilg.hpp"
#include "OknoGlowne.hpp"

ZakladkaWilg::ZakladkaWilg(QWidget *wRodzic): QWidget(wRodzic){
    QVBoxLayout *wOrganizerWer1 = new QVBoxLayout;
    _wWykWilgGroupBox = new QGroupBox(this);
    _wWykWilg         = new WykresWilg(&(OknoGlowne::GetwPomiary()->WilgOkr), OknoGlowne::GetwPomiary()->ZWilg);
    _wWykWilgWidok    = new WykresWidok(_wWykWilg->getWykres(), _wWykWilgGroupBox);

    _wWykWilgGroupBox->setTitle(tr("Wykres wilgotności"));
    _wWykWilgGroupBox->adjustSize();

    wOrganizerWer1->addWidget(_wWykWilgWidok);
    _wWykWilgGroupBox->setLayout(wOrganizerWer1);

    QGridLayout *wOrganizerGrid = new QGridLayout;

    _wInfoOgolneGroupBox = new QGroupBox(this);
    QFormLayout *wOrganizerForm1 = new QFormLayout;
    _wEditMaxWilg  = new QLineEdit(_wInfoOgolneGroupBox);
    _wEditMaxWilg->setReadOnly(true);
    _wEditMaxWilg->setAlignment(Qt::AlignHCenter);
    _wEditMinWilg   = new QLineEdit(_wInfoOgolneGroupBox);
    _wEditMinWilg->setReadOnly(true);
    _wEditMinWilg->setAlignment(Qt::AlignHCenter);
    _wEditAktWilg   = new QLineEdit(_wInfoOgolneGroupBox);
    _wEditAktWilg->setReadOnly(true);
    _wEditAktWilg->setAlignment(Qt::AlignHCenter);
    _wEditMaxDzisWilg   = new QLineEdit(_wInfoOgolneGroupBox);
    _wEditMaxDzisWilg->setReadOnly(true);
    _wEditMaxDzisWilg->setAlignment(Qt::AlignHCenter);
    _wEditMinDzisWilg   = new QLineEdit(_wInfoOgolneGroupBox);
    _wEditMinDzisWilg->setReadOnly(true);
    _wEditMinDzisWilg->setAlignment(Qt::AlignHCenter);

    wOrganizerForm1->addRow(new QLabel(tr("Zarejestrowane maksimum:")), _wEditMaxWilg);
    wOrganizerForm1->addRow(new QLabel(tr("Zarejestrowane minimum:")),  _wEditMinWilg);
    wOrganizerForm1->addRow(new QLabel(tr("Aktualna wilgotność:")), _wEditAktWilg);
    wOrganizerForm1->addRow(new QLabel(tr("Maksimum dzisiaj:")), _wEditMaxDzisWilg);
    wOrganizerForm1->addRow(new QLabel(tr("Minimum dzisiaj:")), _wEditMinDzisWilg);
    _wInfoOgolneGroupBox->setLayout(wOrganizerForm1);
    _wInfoOgolneGroupBox->setTitle(tr("Podstawowe dane"));
    _wInfoOgolneGroupBox->adjustSize();

    _wDaneStatGroupBox = new QGroupBox(this);
    QFormLayout *wOrganizerForm2 = new QFormLayout;
    _wEditAvgWilg  = new QLineEdit(_wDaneStatGroupBox);
    _wEditAvgWilg->setReadOnly(true);
    _wEditAvgWilg->setAlignment(Qt::AlignHCenter);
    _wEditMedWilg   = new QLineEdit(_wDaneStatGroupBox);
    _wEditMedWilg->setReadOnly(true);
    _wEditMedWilg->setAlignment(Qt::AlignHCenter);
    _wEditStdWilg   = new QLineEdit(_wDaneStatGroupBox);
    _wEditStdWilg->setReadOnly(true);
    _wEditStdWilg->setAlignment(Qt::AlignHCenter);
    _wEditVarWilg   = new QLineEdit(_wDaneStatGroupBox);
    _wEditVarWilg->setReadOnly(true);
    _wEditVarWilg->setAlignment(Qt::AlignHCenter);

    wOrganizerForm2->addRow(new QLabel(tr("Średnia:")), _wEditAvgWilg);
    wOrganizerForm2->addRow(new QLabel(tr("Mediana:")),  _wEditMedWilg);
    wOrganizerForm2->addRow(new QLabel(tr("Odchylenie standardowe:")), _wEditStdWilg);
    wOrganizerForm2->addRow(new QLabel(tr("Wariancja:")), _wEditVarWilg);

    _wDaneStatGroupBox->setLayout(wOrganizerForm2);
    _wDaneStatGroupBox->setTitle(tr("Dane Statystyczne"));
    _wDaneStatGroupBox->adjustSize();

    _wPoleWyboruGroupBox = new QGroupBox(this);
    QGridLayout *wOrganizerGrid2 = new QGridLayout;
    _wComboBoxZakres = new QComboBox(_wPoleWyboruGroupBox);
    _wComboBoxZakres->addItem("Podaj Datę", 1);
    _wComboBoxZakres->addItem("Dzisiaj", 2);
    _wComboBoxZakres->addItem("Tydzień", 3);
    _wComboBoxZakres->addItem("Miesiąc", 4);
    _wLabComboBoxZakres = new QLabel("Albo zmień tryb wyświetlania:", _wPoleWyboruGroupBox);
    _wLabKalendarzOD = new QLabel("Wybierz datę początkową: ");
    _wLabKalendarzDO = new QLabel("Wybierz datę końcową: ");
    _wKalendarzOD = new QCalendarWidget;
    _wKalendarzOD->setMinimumDate(QDate(1900, 1, 1));
    _wKalendarzOD->setMaximumDate(QDate(3000, 1, 1));
    _wKalendarzOD->setGridVisible(true);
    _wKalendarzDO = new QCalendarWidget;
    _wKalendarzDO->setMinimumDate(QDate(1900, 1, 1));
    _wKalendarzDO->setMaximumDate(QDate(3000, 1, 1));
    _wKalendarzDO->setGridVisible(true);
    wOrganizerGrid2->addWidget(_wLabKalendarzOD, 0, 0, 1, 4);
    wOrganizerGrid2->addWidget(_wKalendarzOD, 1, 0, 4, 4);
    wOrganizerGrid2->addWidget(_wLabKalendarzDO, 5, 0, 1, 4);
    wOrganizerGrid2->addWidget(_wKalendarzDO, 6, 0, 4, 4);
    wOrganizerGrid2->addWidget(_wLabComboBoxZakres, 10, 0, 1, 4);

    _wZatwierdz = new QPushButton("Zatwierdź",_wPoleWyboruGroupBox);
    wOrganizerGrid2->addWidget(_wZatwierdz, 11, 3, 1, 1);

    wOrganizerGrid2->addWidget(_wComboBoxZakres, 11, 0, 1, 3);
    _wPoleWyboruGroupBox->setLayout(wOrganizerGrid2);

    _wPoleWyboruGroupBox->setTitle(tr("Zakres wyświetlania"));

    _wPredykcjaGroubBox = new QGroupBox(this);
    _wCheckBox = new QCheckBox(tr("metoda Browna"), _wPredykcjaGroubBox);
    QVBoxLayout *wOrganizerWer3 = new QVBoxLayout;
    wOrganizerWer3->addWidget(_wCheckBox);
    _wPredykcjaGroubBox->setLayout(wOrganizerWer3);
    _wPredykcjaGroubBox->setTitle(tr("Pokaż predykcję"));

    _wInfoOgolneGroupBox->setMaximumHeight(_wInfoOgolneGroupBox->height());
    _wDaneStatGroupBox->setMaximumHeight(_wInfoOgolneGroupBox->height());
    _wPoleWyboruGroupBox->setMinimumWidth(250);
    _wPredykcjaGroubBox->setMinimumWidth(250);

    wOrganizerGrid->addWidget(_wWykWilgGroupBox, 0, 0, 4, 2);
    wOrganizerGrid->addWidget(_wInfoOgolneGroupBox, 4, 0, 2, 1);
    wOrganizerGrid->addWidget(_wDaneStatGroupBox, 4, 1, 2, 1);
    wOrganizerGrid->addWidget(_wPoleWyboruGroupBox, 0, 2, 5, 1);
    wOrganizerGrid->addWidget(_wPredykcjaGroubBox, 5, 2, 1, 1);
    wOrganizerGrid->setColumnStretch(0,1);
    wOrganizerGrid->setColumnStretch(1,1);
    wOrganizerGrid->setRowStretch(0,1);
    wOrganizerGrid->setRowStretch(1,1);
    wOrganizerGrid->setRowStretch(2,1);
    wOrganizerGrid->setRowStretch(3,1);

    this->setLayout(wOrganizerGrid);

    connect(_wCheckBox, SIGNAL(stateChanged(int)), _wWykWilg, SLOT(GdyChcePredykcje(int)));
    connect(_wZatwierdz, SIGNAL(clicked(bool)), this, SLOT(GdyZatwierdzonoWybor(bool)));
    connect(this, SIGNAL(PrzekazStatus(QString)), OknoGlowne::GetswOknoG(), SLOT(OdbierzNapisStatusu(QString)));

}

void ZakladkaWilg::uaktualnij(){
  _wWykWilg->uaktualnij();
  _wEditMaxWilg->setText(QString::number(OknoGlowne::GetwPomiary()->MaxW,'f',2) + " %RH");
  _wEditMinWilg->setText(QString::number(OknoGlowne::GetwPomiary()->MinW,'f',2) + " %RH");
  _wEditAktWilg->setText(QString::number(OknoGlowne::GetwPomiary()->AktW,'f',2) + " %RH");
  _wEditMaxDzisWilg->setText(QString::number(OknoGlowne::GetwPomiary()->MaxWDzis,'f',2) + " %RH");
  _wEditMinDzisWilg->setText(QString::number(OknoGlowne::GetwPomiary()->MinWDzis,'f',2) + " %RH");
  _wEditAvgWilg->setText(QString::number(OknoGlowne::GetwPomiary()->AvgWOkr,'f',2) + " %RH");
  _wEditMedWilg->setText(QString::number(OknoGlowne::GetwPomiary()->MedWilg(),'f',2) + " %RH");
  _wEditStdWilg->setText(QString::number(OknoGlowne::GetwPomiary()->StdWOkr,'f',2) + " %RH");
  _wEditVarWilg->setText(QString::number(OknoGlowne::GetwPomiary()->VarWOkr,'f',2) + " %RH");
}

void ZakladkaWilg::GdyZatwierdzonoWybor(bool checked __attribute__((unused))){
    OknoGlowne::GetwPomiary()->WilgFlag = 1;
    switch(_wComboBoxZakres->currentIndex()){
      case 0:
        OknoGlowne::GetwPomiary()->ZWilg = Data;
        OknoGlowne::GetwPomiary()->DataOD = _wKalendarzOD->selectedDate();
        OknoGlowne::GetwPomiary()->DataDO = _wKalendarzDO->selectedDate();
        break;
      case 1:
        OknoGlowne::GetwPomiary()->ZWilg = Dzisiaj;
        break;
      case 2:
        OknoGlowne::GetwPomiary()->ZWilg = Tydzien;
        break;
      case 3:
        OknoGlowne::GetwPomiary()->ZWilg = Miesiac;
    }
    emit PrzekazStatus("Pytam baze");
    OknoGlowne::GetwMutex()->lock();
    OknoGlowne::GetwWatekBD()->AsynPolaczenie();
    OknoGlowne::GetwMutex()->unlock();
    emit PrzekazStatus("Odpytalem");
}

