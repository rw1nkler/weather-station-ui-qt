/*!
 * \file
 * \brief Zawiera źródła klasy OknoGlowne
 *
 * Plik źródłowy klasy OknoGlowne, które stanowi okno główne aplikacji.
 */

#include "OknoGlowne.hpp"
#include "ui_oknoglowne.h"

#include <QApplication>
#include <QWidget>
#include <QMainWindow>
#include <QTabWidget>
#include <QString>
#include <QStatusBar>
#include <iostream>
#include <sstream>

#include "Zakladki.hpp"
using namespace std;

StrPom* OknoGlowne::sPomiary = NULL;
WatekBazyDanych* OknoGlowne::swWatekBD = NULL;
QMutex* OknoGlowne::sMutex = NULL;
OknoGlowne* OknoGlowne::swOknoG = NULL;

/*!
 * W konstruktorze tworzone są główne widgety okna takie jak menu, pasek statusu ale także akcje dla menu i widget zakładek.
 * W nim powstaje także wątek służący do komunikacji z bazą danych. Następuje także połączenie sygnału WatekBazyDanych::PobranoDane() ze slotem Zakladki::GdyPobranoDane(),
 * co jest mechanizmym informującym główny wątek o pojawieniu się nowych danych i wymuszającym odświerzenie zakładek.
 *
 * \param[in] wRodzic - Wskaźnik na obiekt rodzica. Tutaj NULL.
 */
OknoGlowne::OknoGlowne(QWidget *wRodzic): QMainWindow(wRodzic)/*, ui(new Ui::OknoGlowne)*/{
  sPomiary = &_Pomiary;
  sMutex = &_MutexGdyOdczytujePomiary;
  swOknoG = this;
  setWindowTitle("Stacja Meteorologiczna");

  _wWatekBD = new WatekBazyDanych(this);
  swWatekBD = _wWatekBD;

  UtworzPasekZakladek();
  UtworzAkcje();
  UtworzMenu();
  UtworzPasekStatusu();

  connect(_wWatekBD, SIGNAL(PobranoDane()), _wZakladki, SLOT(GdyPobranoDane()));

  resize(500, 500);
}

/*!
 * Tworzy widget zakładek i łączy sygnał Zakladki::currentChanged(int) ze slotem OknoGlowne::OdbierzNapisStatusu(int).
 */
void OknoGlowne::UtworzPasekZakladek(){
  _wZakladki = new Zakladki(this);
  setCentralWidget(_wZakladki);
  connect(_wZakladki, SIGNAL(currentChanged(int)), this, SLOT(OdbierzNapisStatusu(int)));
}

/*!
 * Tworzy pasek statusu i ustawia w nim napis "Start aplikacji".
 */
void OknoGlowne::UtworzPasekStatusu(){
  setStatusBar(new QStatusBar());
  statusBar()->showMessage(QObject::tr("Start aplikacji"));
}

/*!
 * Tworzy akcje:
 * - Zamknij aplikację
 * - Pobierz Dane
 * - Wyświetl informacje
 * - Pomoc
 * które uruchamiają się po wybraniu odpoiedniej opcji z menu.
 */
void OknoGlowne::UtworzAkcje(){
  _wAkcjaZamknij = new QAction(tr("&Zamknij"), this);
  _wAkcjaZamknij->setShortcut(QKeySequence::Close);
  _wAkcjaZamknij->setStatusTip(tr("Kliknij żeby zakmnąć program."));
  connect(_wAkcjaZamknij, SIGNAL(triggered()), this, SLOT(CzyMoznaZamknac()));

  _wAkcjaPobierzDane = new QAction(tr("Pobierz &Dane"), this);
  _wAkcjaPobierzDane->setShortcut(QKeySequence::ZoomIn);
  _wAkcjaPobierzDane->setStatusTip(tr("Kliknij żeby pobrać surowe dane z bazy danych."));
  //connect(_wAkcjaPobierzDane, SIGNAL(triggered()), this, NULL);

  _wAkcjaInformacje = new QAction(tr("Wyświetl I&nformacje"), this);
  _wAkcjaInformacje->setStatusTip(tr("Kliknij żeby wyświetlić informacje o aplikacji"));
  connect(_wAkcjaInformacje, SIGNAL(triggered()), this, SLOT(WyswietlInfo()));

  _wAkcjaPomoc = new QAction(tr("Wyświetl Po&moc"), this);
  _wAkcjaPomoc->setShortcut(QKeySequence::HelpContents);
  _wAkcjaPomoc->setStatusTip(tr("Kliknij żeby wyświetlić pomoc."));
  connect(_wAkcjaPomoc, SIGNAL(triggered()), this, SLOT(WyswietlPomoc()));
}

/*!
 * Tworzy menu składające się z pól:
 * - Plik
 *   -# Zamknij
 *   -# Pobierz Dane
 * - Informacje
 * - Pomoc
 */
void OknoGlowne::UtworzMenu(){
  _wMenu = menuBar()->addMenu(tr("&Plik"));
    _wMenu->addAction(_wAkcjaZamknij);
    _wMenu->addAction(_wAkcjaPobierzDane);

  _wMenu = menuBar()->addMenu(tr("&Informacje"));
    _wMenu->addAction(_wAkcjaInformacje);

  _wMenu = menuBar()->addMenu(tr("P&omoc"));
    _wMenu->addAction(_wAkcjaPomoc);
}

OknoGlowne::~OknoGlowne(){
}

/*!
 * \param[in] index - Index pochodzący od widgetu QTabWidget, określający która zakładka jest aktywna.
 */
void OknoGlowne::OdbierzNapisStatusu(int index){
  switch(index){
  case 0:
    statusBar()->showMessage(tr("Ogólne"));
    break;
  case 1:
    statusBar()->showMessage(tr("Temperatura"));
    break;
  case 2:
    statusBar()->showMessage(tr("Wilgotność"));
    break;
  case 3:
    statusBar()->showMessage(tr("Ciśnienie"));
    break;
  }
}

/*!
 * \param[in] tekst - Treść komunikatu wyświetlana w pasku statusowym.
 */
void OknoGlowne::OdbierzNapisStatusu(QString tekst){
    statusBar()->showMessage(tekst);
}

/*!
 * Tworzy QMessageBox i wyświetla pytanie "Czy na pewno chcesz zamknąć program?
 * Jeżeli użytkownik kliknie "tak" aplikacja jest zamykana, jeżeli kliknie "nie" następuje powrót do aplikacji.
 */
void OknoGlowne::CzyMoznaZamknac(){
  QMessageBox::StandardButton Answer = QMessageBox::question(this,tr("Pytanie"),
                                       tr("Czy na pewno chcesz zamknąć program?"),
                                       QMessageBox::Yes | QMessageBox::No,
                                       QMessageBox::No );
  if(Answer == QMessageBox::Yes){
    qApp->quit();
  }
}

void OknoGlowne::WyswietlInfo(){
  QMessageBox::information(this,tr("Informacja"),
  tr("Autor: Robert Winkler \r\n Wersja aplikacji 1.0"),
  QMessageBox::Ok,
  QMessageBox::Ok );
}

void OknoGlowne::WyswietlPomoc(){
    QMessageBox::information(this, tr("Pomoc"),
                             tr("1. Przełączaj się pomiędzy zakładkami aby uzyskać szczegółowe informacje o danej wartości. \r\n"
                                "2. Korzystaj z widgetu kalendarza do wyboru daty, aby wybrać datę początkową i końową. \r\n"
                                "3. Zmiany trybu wyświetlania możesz dokonać za pomocą rozwijanej listy pod widgetem kalendarza.\r\n"
                                "4. Predykcję możesz wyświetlić klikając w pole METODA BROWNA."),
                             QMessageBox::Ok,
                             QMessageBox::Ok);
}

/*!
 * Getter statycznego pola sPomiary przechowującego wskaźnik na strukturą pomiarową.
 * Należy korzystać z gettera celem uniknięcia skomplikowanego przekazywania wskaźnika.
 *
 * \return Wskaźnik na strukturę pomiarową.
 */
StrPom* OknoGlowne::GetwPomiary(){
    return OknoGlowne::sPomiary;
}

/*!
 * Getter statycznego pola swWatek przechowującego wskaźnik na wątek obsługujący komunikację z bazą danych.
 * Należy korzystać z gettera celem uniknięcia skomplikowanego przekazywania wskaźnika.
 *
 * \return Wskaźnik na handler do wątku obsługującego komunikację z bazą danych.
 */
WatekBazyDanych* OknoGlowne::GetwWatekBD(){
    return swWatekBD;
}

/*!
 * Getter statycznego pola sMutex przechowującego wskaźnik na mutex używany podczas korzystania ze struktury pomiarowej.
 * Należy korzystać z gettera celem uniknięcia skomplikowanego przekazywania wskaźnika.
 *
 * \return Wskaźnik na mutex używany podczas korzystania ze struktury pomiarowej.
 */
QMutex* OknoGlowne::GetwMutex(){
    return sMutex;
}

/*!
 * Getter statycznego pola swOknoG przechowującego wskaźnik na okno główne.
 * Należy korrzystać z gettera celem uniknięcia skomplikowanego przekazywania wskaźnika.
 *
 * \return Wskaźnik na okno główne.
 */
OknoGlowne* OknoGlowne::GetswOknoG(){
    return swOknoG;
}
