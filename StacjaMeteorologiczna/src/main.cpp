/*!
 * \file
 * \brief Główny plik projektu Stacji Meteorologicznej.
 *
 * W nim tworzy się okno główne programu i uruchamia pętla zdarzeń.
 */


#include "OknoGlowne.hpp"
#include <QApplication>
#include "Zakladki.hpp"

int main(int argc, char *argv[]){
    QApplication App(argc, argv);
    OknoGlowne Okno;
    Okno.show();

    return App.exec();
}
