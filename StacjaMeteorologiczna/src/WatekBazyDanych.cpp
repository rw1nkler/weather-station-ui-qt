#include "WatekBazyDanych.hpp"
#include "OknoGlowne.hpp"
#include "StrPom.hpp"

#include <iostream>

WatekBazyDanych::WatekBazyDanych(QObject *wRodzic): QThread(wRodzic){
  _quit = 0;
  _asynch = 0;

  if(!isRunning())
    start();
}

WatekBazyDanych::~WatekBazyDanych(){
  _quit = 1;
  wait();
  OknoGlowne::GetwMutex()->unlock();
  cout << "Destruktor watku" << endl;
}


void WatekBazyDanych::run(){
  StrPom *wPomiary = OknoGlowne::GetwPomiary();
  int error = 0;
  int licznikBledow = 0;
  while(!_quit){
    OknoGlowne::GetwMutex()->lock();
    error += Baza.PobTemp(wPomiary);
    error += Baza.PobCisn(wPomiary);
    error += Baza.PobWilg(wPomiary);
    OknoGlowne::GetwMutex()->unlock();

   if(error >= 0){
      int opoznienie = 0;
      emit PobranoDane();
      while(opoznienie != 5*60*1000){
        msleep(1);
        if(_asynch == 1){
            OknoGlowne::GetwMutex()->lock();
            if(wPomiary->TempFlag) error += Baza.PobTemp(wPomiary);
            if(wPomiary->CisnFlag) error += Baza.PobCisn(wPomiary);
            if(wPomiary->WilgFlag) error += Baza.PobWilg(wPomiary);
            wPomiary->TempFlag = 0;
            wPomiary->CisnFlag = 0;
            wPomiary->WilgFlag = 0;
            OknoGlowne::GetwMutex()->unlock();
            _asynch = 0;
            emit PobranoDane();
        }
        ++opoznienie;
      }
      licznikBledow = 0;
    }
    else{
      ++licznikBledow;
       sleep(10);
      cout << "Blad" << endl;
      if(licznikBledow >= 10 && licznikBledow < 20){
        cout << "Nie mozna polaczyc z baza danych" << endl;
        sleep(20);
      }
      else if(licznikBledow >= 20){
          cout << "Baza" << endl;
          ZamknijWatek();
      }
    }
  }
}

void WatekBazyDanych::ZamknijWatek(){
  _quit = 1;
}

void WatekBazyDanych::AsynPolaczenie(){
  _asynch = 1;
}
