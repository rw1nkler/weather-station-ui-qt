var NAVTREE =
[
  [ "StacjaMeteorologiczna", "index.html", [
    [ "Aplikacja wizualizująca dane ze Stacji Meteorologicznej", "index.html", null ],
    [ "Przestrzenie nazw", null, [
      [ "Lista przestrzeni nazw", "namespaces.html", "namespaces" ]
    ] ],
    [ "Klasy", "annotated.html", [
      [ "Lista klas", "annotated.html", "annotated_dup" ],
      [ "Indeks klas", "classes.html", null ],
      [ "Hierarchia klas", "hierarchy.html", "hierarchy" ],
      [ "Składowe klas", "functions.html", [
        [ "All", "functions.html", "functions_dup" ],
        [ "Funkcje", "functions_func.html", null ],
        [ "Zmienne", "functions_vars.html", null ],
        [ "Wyliczenia", "functions_enum.html", null ],
        [ "Wartości wyliczeń", "functions_eval.html", null ]
      ] ]
    ] ],
    [ "Pliki", null, [
      [ "Lista plików", "files.html", "files" ],
      [ "Składowe plików", "globals.html", [
        [ "All", "globals.html", null ],
        [ "Funkcje", "globals_func.html", null ],
        [ "Wyliczenia", "globals_enum.html", null ],
        [ "Wartości wyliczeń", "globals_eval.html", null ]
      ] ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"_baza_danych_8cpp.html",
"class_zakladka_cisn.html#abfd0349e97af6bad770263883ca41535"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';