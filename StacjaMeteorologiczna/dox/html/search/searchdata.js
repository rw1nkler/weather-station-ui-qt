var indexSectionsWithContent =
{
  0: "_abcdgimoprstuvwz~",
  1: "bopswz",
  2: "u",
  3: "bmopswz",
  4: "abcgmoprstuwz~",
  5: "_abcdmstvwz",
  6: "ip",
  7: "cdmtw",
  8: "a"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "enums",
  7: "enumvalues",
  8: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Klasy",
  2: "Przestrzenie nazw",
  3: "Pliki",
  4: "Funkcje",
  5: "Zmienne",
  6: "Wyliczenia",
  7: "Wartości wyliczeń",
  8: "Strony"
};

