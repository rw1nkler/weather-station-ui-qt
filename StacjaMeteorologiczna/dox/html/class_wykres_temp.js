var class_wykres_temp =
[
    [ "WykresTemp", "class_wykres_temp.html#a2f4ac5cb81412da9da9e99fafd7f1d84", null ],
    [ "~WykresTemp", "class_wykres_temp.html#a85ac54efc957c0a00d45ed027dba8765", null ],
    [ "GdyChcePredykcje", "class_wykres_temp.html#a6beb775c44b10b2e898f7fabe9d4931e", null ],
    [ "getNazwa", "class_wykres_temp.html#a2ca2d5748804265abaa7cccbe15e21df", null ],
    [ "getWykres", "class_wykres_temp.html#a58ad7d29270be1e787649aafa3c5d4b6", null ],
    [ "setNazwa", "class_wykres_temp.html#a601e1dac3f845c6e33bc1830734db6cf", null ],
    [ "setTitle", "class_wykres_temp.html#a7633788442e1715b5267b7a0f026cd79", null ],
    [ "uaktualnij", "class_wykres_temp.html#a82ab860fddaa6289cd4e897ef7dbea22", null ],
    [ "_osX", "class_wykres_temp.html#a5454e32f5290de831c13626578898f11", null ],
    [ "_osY", "class_wykres_temp.html#a1cfdf1eb16a5c12615086fcb4a1692c8", null ],
    [ "_wNazwa", "class_wykres_temp.html#a4a1829623eaed5683eaa2f6c8bfc0262", null ],
    [ "_wPomiary", "class_wykres_temp.html#ab5b91ebcc5263ff9f3c7991146cbac5a", null ],
    [ "_wPredykcja", "class_wykres_temp.html#ae5d4a2ecfca0ce83efcf507811477c93", null ],
    [ "_wSeriaDanych", "class_wykres_temp.html#a16c6afdd10b6e639db2707b266ce3ddb", null ],
    [ "_wWykres", "class_wykres_temp.html#a21a99f47a7f01750eb0dd56e298c5fd7", null ],
    [ "_Zakres", "class_wykres_temp.html#a98140921312f459fc9863f1b39e7ab3a", null ]
];