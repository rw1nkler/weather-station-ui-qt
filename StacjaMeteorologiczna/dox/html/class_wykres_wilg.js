var class_wykres_wilg =
[
    [ "WykresWilg", "class_wykres_wilg.html#a17f59525f6db6ba888b0249578f98609", null ],
    [ "~WykresWilg", "class_wykres_wilg.html#a90f09499472cece83cf290224bbba812", null ],
    [ "GdyChcePredykcje", "class_wykres_wilg.html#aedeb7ce332d920a078384eeae383a91e", null ],
    [ "getNazwa", "class_wykres_wilg.html#a4882812705177299820e063870b10e99", null ],
    [ "getWykres", "class_wykres_wilg.html#a092eb8b4f6ecec4c8ee8d219399aa89d", null ],
    [ "setNazwa", "class_wykres_wilg.html#afed7effca27aec116d1cd0efb9a8c8a3", null ],
    [ "setTitle", "class_wykres_wilg.html#abd720ac1d19c3a46e948c0c5001226cc", null ],
    [ "uaktualnij", "class_wykres_wilg.html#ab81b6425d9ea3020e7105f874b551c11", null ],
    [ "_osX", "class_wykres_wilg.html#a9ef8478bca3a957b457c01eca21355c2", null ],
    [ "_osY", "class_wykres_wilg.html#aca5149e4d809aee555b162475bd30637", null ],
    [ "_wNazwa", "class_wykres_wilg.html#af9602424f2355412bdaa697761de197e", null ],
    [ "_wPomiary", "class_wykres_wilg.html#a85e25acf9b7f20092d1de3be6836ced4", null ],
    [ "_wPredykcja", "class_wykres_wilg.html#a80cb1df2e1c21a77ddcf415e61f3882a", null ],
    [ "_wSeriaDanych", "class_wykres_wilg.html#a472263293cf41f4ebfd44da0595862cd", null ],
    [ "_wWykres", "class_wykres_wilg.html#a760a25fd579365add04c16308426d64c", null ],
    [ "_Zakres", "class_wykres_wilg.html#a7cde9e0d236e0e891fc814e2a4ffb56b", null ]
];