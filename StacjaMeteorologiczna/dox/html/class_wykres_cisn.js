var class_wykres_cisn =
[
    [ "WykresCisn", "class_wykres_cisn.html#a60a85e1f442b0f3031c6ee726c78f858", null ],
    [ "~WykresCisn", "class_wykres_cisn.html#a9385ee52e40f7ed0aa31374edf88a663", null ],
    [ "GdyChcePredykcje", "class_wykres_cisn.html#a8d84b3b3ac5e2a7568871940aefc5752", null ],
    [ "getNazwa", "class_wykres_cisn.html#ae12d1a2da94cfb33deffcd1421cc0045", null ],
    [ "getWykres", "class_wykres_cisn.html#a72cdd7e410f75cd8da2b82beb0734592", null ],
    [ "setNazwa", "class_wykres_cisn.html#ac048fa79e882aed1d42b5fcf1f17a3e3", null ],
    [ "setTitle", "class_wykres_cisn.html#a7e67675da9aa798108cdd30492e41411", null ],
    [ "uaktualnij", "class_wykres_cisn.html#ad6487201c8904174395a86d3d190e389", null ],
    [ "_osX", "class_wykres_cisn.html#aafbc97288c88728e8fee208b1243d0b3", null ],
    [ "_osY", "class_wykres_cisn.html#ae08e4533f4b058af5cff5bad9a6f3adc", null ],
    [ "_wNazwa", "class_wykres_cisn.html#aa014cc9ed756ae6abb670d321a43238f", null ],
    [ "_wPomiary", "class_wykres_cisn.html#ab5fc237fa41a72ae9dc67c53eb2957e5", null ],
    [ "_wPredykcja", "class_wykres_cisn.html#a0b8f7d5335bc0df92010d0cee9d6aa55", null ],
    [ "_wSeriaDanych", "class_wykres_cisn.html#a46f090df2741fe5cb9b4a4c9858921b4", null ],
    [ "_wWykres", "class_wykres_cisn.html#abc8d14a0a3dac2054d1b2e6afe08f53c", null ],
    [ "_Zakres", "class_wykres_cisn.html#a7179911f89f66872556dad5ff335c3e0", null ]
];