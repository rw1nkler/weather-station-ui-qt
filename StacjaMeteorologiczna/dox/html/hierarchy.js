var hierarchy =
[
    [ "BazaDanych", "class_baza_danych.html", null ],
    [ "PomiaryTablica", "class_pomiary_tablica.html", null ],
    [ "QChartView", null, [
      [ "WykresWidok", "class_wykres_widok.html", null ]
    ] ],
    [ "QMainWindow", null, [
      [ "OknoGlowne", "class_okno_glowne.html", null ]
    ] ],
    [ "QObject", null, [
      [ "WykresCisn", "class_wykres_cisn.html", null ],
      [ "WykresTemp", "class_wykres_temp.html", null ],
      [ "WykresWilg", "class_wykres_wilg.html", null ]
    ] ],
    [ "QTabWidget", null, [
      [ "Zakladki", "class_zakladki.html", null ]
    ] ],
    [ "QThread", null, [
      [ "WatekBazyDanych", "class_watek_bazy_danych.html", null ]
    ] ],
    [ "QWidget", null, [
      [ "ZakladkaCisn", "class_zakladka_cisn.html", null ],
      [ "ZakladkaOgolne", "class_zakladka_ogolne.html", null ],
      [ "ZakladkaTemp", "class_zakladka_temp.html", null ],
      [ "ZakladkaWilg", "class_zakladka_wilg.html", null ]
    ] ],
    [ "StrPom", "class_str_pom.html", null ],
    [ "WpisPomiarowy", "struct_wpis_pomiarowy.html", null ]
];