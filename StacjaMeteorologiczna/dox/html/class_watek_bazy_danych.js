var class_watek_bazy_danych =
[
    [ "Pobierz", "class_watek_bazy_danych.html#a3a4c0b89ab48e78431be5d06da45a441", [
      [ "Temperatura", "class_watek_bazy_danych.html#a3a4c0b89ab48e78431be5d06da45a441a1993cca6563268711425f1a3a3288d60", null ],
      [ "Cisnienie", "class_watek_bazy_danych.html#a3a4c0b89ab48e78431be5d06da45a441a3e49bc5758a4ca13b9b0070b0c2de750", null ],
      [ "Wilgotnosc", "class_watek_bazy_danych.html#a3a4c0b89ab48e78431be5d06da45a441a7f204b2c1c773b861b1c83211961ef3d", null ]
    ] ],
    [ "WatekBazyDanych", "class_watek_bazy_danych.html#a8d20d109807bf4e52de39997cfa228b0", null ],
    [ "~WatekBazyDanych", "class_watek_bazy_danych.html#a2735ad3a22c5dce1a1db17e75226ac61", null ],
    [ "AsynPolaczenie", "class_watek_bazy_danych.html#aa7b27619231a044dccdfb6bd7e4007bb", null ],
    [ "PobranoDane", "class_watek_bazy_danych.html#a19f4e8bd5f33309fc5362e5f5de3d126", null ],
    [ "run", "class_watek_bazy_danych.html#ac77ecda24094899d0c95b3cd6eb33f90", null ],
    [ "ZamknijWatek", "class_watek_bazy_danych.html#a94b69b06d24773c1cb7129229988dfcc", null ],
    [ "_asynch", "class_watek_bazy_danych.html#a1102f2fb39eeff549b223ed503e14328", null ],
    [ "_quit", "class_watek_bazy_danych.html#a33b1bc4f448136a1e9f138eed0f50d41", null ],
    [ "Baza", "class_watek_bazy_danych.html#a66ca3a1a68b8a8b49e3305f192384d2b", null ],
    [ "mutex", "class_watek_bazy_danych.html#a25a86ec1177788ad555b2ba97556d040", null ]
];