#ifndef ZAKLADKATEMP_HPP
#define ZAKLADKATEMP_HPP

#include <QWidget>
#include <QGroupBox>
#include <QLabel>
#include <QLineEdit>
#include <QComboBox>
#include <QCheckBox>
#include <QCalendarWidget>
#include <QPushButton>

#include "WykresTemp.hpp"
#include "WykresWidok.hpp"
#include "StrPom.hpp"

using namespace std;
/*!
 * \brief Definicja klasy ZakladkaTemp
 *
 * Zawiera wszystko co znajduje się w zakładce szczegółowej dotyczącej temperatury:
 * - Wykres
 * - Podstawowe Dane
 * - Informacje statystyczne
 * - Mechanizmy wyboru zakresu danych
 * - Predykcję
 */
class ZakladkaTemp: public QWidget{
  Q_OBJECT
public:
  ZakladkaTemp(QWidget *wRodzic);
  /*!
   * \brief Metoda służaca do uaktualniania danych w zakładce po nadejścu nowych danych
   */
  void uaktualnij();
public slots:
  /*!
     * \brief Slot używany po zatwierdzeniu przez urzytkownika zakresu wyświetlanych danych w zakładce szczegółowej
     */
    void GdyZatwierdzonoWybor(bool checked);
signals:
    /*!
   * \brief Sygnał używany do przekazania napisu statusu do paska statusu okna głównego
   */
  void PrzekazStatus(QString tekst);
private:
  QGroupBox *_wWykTempGroupBox;
  QGroupBox *_wInfoOgolneGroupBox;
  QGroupBox *_wDaneStatGroupBox;
  QGroupBox *_wPoleWyboruGroupBox;
  QGroupBox *_wPredykcjaGroubBox;
  WykresTemp *_wWykTemp;
  WykresWidok *_wWykTempWidok;
  QCalendarWidget *_wKalendarzOD;
  QCalendarWidget *_wKalendarzDO;

  QLineEdit *_wEditMaxTemp;
  QLineEdit *_wEditMinTemp;
  QLineEdit *_wEditAktTemp;
  QLineEdit *_wEditMaxDzisTemp;
  QLineEdit *_wEditMinDzisTemp;

  QLineEdit *_wEditAvgTemp;
  QLineEdit *_wEditMedTemp;
  QLineEdit *_wEditStdTemp;
  QLineEdit *_wEditVarTemp;

  QComboBox *_wComboBoxZakres;
  QLabel *_wLabComboBoxZakres;

  QCheckBox *_wCheckBox;
  QLabel *_wLabCheckBox;
  QLabel *_wLabKalendarzOD;
  QLabel *_wLabKalendarzDO;
  QPushButton *_wZatwierdz;
};


#endif /* ZAKLADKATEMP_HPP */
