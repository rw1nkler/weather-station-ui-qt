#ifndef ZAKLADKAOGOLNE_HPP
#define ZAKLADKAOGOLNE_HPP

#include <QWidget>
#include <QGroupBox>
#include <QLabel>
#include <QLineEdit>

#include "WykresTemp.hpp"
#include "WykresCisn.hpp"
#include "WykresWilg.hpp"
#include "WykresWidok.hpp"
#include "StrPom.hpp"

using namespace std;

class ZakladkaOgolne: public QWidget{
  Q_OBJECT
public:
  ZakladkaOgolne(QWidget *wRodzic);
  WykresTemp* getWykresTemp();
  WykresWilg* getWykresWilg();
  WykresCisn* getWykresCisn();
  void uaktualnij();
private:
  QGroupBox *_wInfoGroupBox;
  QLabel *_wIdCzujnika;
  QLabel *_wUlica;
  QLabel *_wKodPocztowy;

  QGroupBox *_wDaneGroupBox;
  QLineEdit *_wEditMaksTemp;
  QLineEdit *_wEditMinTemp;
  QLineEdit *_wEditAktTemp;
  QLineEdit *_wEditAktWilg;
  QLineEdit *_wEditAktCisn;

  QGroupBox *_wWykGroupBox;
  WykresTemp *_wWykTemp;
  WykresWilg *_wWykWilg;
  WykresCisn *_wWykCisn;
  WykresWidok *_wWykTempWidok;
  WykresWidok *_wWykWilgWidok;
  WykresWidok *_wWykCisnWidok;
};


#endif /* ZAKLADKAOGOLNE_HPP */

