#ifndef WATEKBAZYDANYCH_HPP
#define WATEKBAZYDANYCH_HPP

#include <QThread>
#include <QMutex>

#include "BazaDanych.hpp"
#include "StrPom.hpp"

using namespace std;

/*!
 * \brief Definicja klasy WatekBazyDanych.
 *
 * Jest to handler do wątku, który prowadzi komunikację z bazą danych. Główna pętl wątku znajduje się w metodzie run().
 *  Wątek jest odpowiedzialny za wysyłanie zapytań do bazy danych okresowo co ok. 5 minut oraz na wysyłanie zapytań na żadnie urzytkownika.
 */
class WatekBazyDanych :public QThread{
    Q_OBJECT
public:
    /*!
     * \brief Typ wyliczeniowy określający o jaką wartość ma zostać odpytana baza danych
     */
    enum Pobierz {Temperatura, Cisnienie, Wilgotnosc};
    /*!
     * \brief Konstruktor klasy WatekBazyDanych
     */
    WatekBazyDanych(QObject *parent);
    /*!
     * Publiczna metoda umożliwiająca poinforrmowanie wątku o konieczności asynchroniznego odpytania bazy danych.
     */
    void AsynPolaczenie();
    /*!
     * \brief Destruktor klasy WatekBazyDanych
     */
    ~WatekBazyDanych();

    /*!
     * \brief Publiczna metoda umożliwiająca zakończenie pętli głównej wątku
     */
    void ZamknijWatek();
signals:
    /*!
     * \brief Sygnał wysyłany do wątku głównego po pobraniu danych.
     */
    void PobranoDane();

protected:
    /*!
     * \brief Metoda zawierająca pętle główną wątku
     */
    void run() override;

private:
    /*!
     * \brief Pole przechowujący wskaźnik na mutex zakladany podczas pracy na strukturze pomiarowej
     */
    QMutex mutex;
    /*!
     * \brief Klasa zawierająca mechanizmy kontaktu z bza danych
     */
    BazaDanych Baza;

    /*!
     * \brief Pole służące do poinformowania wątku o koniecznośi asynchronicznego pobrania danych
     */
    int _asynch;
    /*!
     * \brief Pole służące do zakończenia wątku
     */
    int _quit;

};

#endif // WATEKBAZYDANYCH_HPP
