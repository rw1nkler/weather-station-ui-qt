#ifndef WYKRESCISN_HPP
#define WYKRESCISN_HPP

#include <QWidget>
#include <QChartGlobal>
#include <QChart>
#include <QChartView>
#include <QSplineSeries>
#include <QLegend>
#include <QPalette>
#include <QString>
#include <QDateTime>
#include <QDateTimeAxis>
#include <QValueAxis>

#include "PomiaryTablica.hpp"
#include "StrPom.hpp"

QT_CHARTS_USE_NAMESPACE

class WykresCisn: public QObject{
  Q_OBJECT
public:
  WykresCisn(PomiaryTablica *wPomiary, IleDni Zakres);
  ~WykresCisn();

  QString& getNazwa() const;
  void setNazwa(QString nazwa);
  QChart* getWykres() const;
  void setTitle(QString title);
  void uaktualnij();
private:
  QString *_wNazwa;
  QChart *_wWykres;
  PomiaryTablica *_wPomiary;
  QLineSeries *_wSeriaDanych;
  QLineSeries *_wPredykcja;
  QDateTimeAxis *_osX;
  QValueAxis *_osY;
  IleDni _Zakres;

public slots:
  void GdyChcePredykcje(int stan);
};

#endif // WYKRESCISN_HPP
