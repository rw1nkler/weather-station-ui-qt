#ifndef STRPOM_HPP
#define STRPOM_HPP

#include "PomiaryTablica.hpp"
#include <QDate>


enum IleDni {Data, Dzisiaj, Tydzien, Miesiac};

class StrPom{
public:

  StrPom();
  ~StrPom();

  QDate DataOD;
  QDate DataDO;

  IleDni ZTemp;
  IleDni ZCisn;
  IleDni ZWilg;

  PomiaryTablica TempDzis;
  PomiaryTablica CisnDzis;
  PomiaryTablica WilgDzis;

  PomiaryTablica TempOkr;
  PomiaryTablica CisnOkr;
  PomiaryTablica WilgOkr;

  int TempFlag;
  int WilgFlag;
  int CisnFlag;

  double MaxTOkr;
  double MinTOkr;
  double AvgTOkr;
  double VarTOkr;
  double StdTOkr;
  double MaxTDzis;
  double MinTDzis;

  double AktT;
  double MaxT;
  double MinT;

  double MaxCOkr;
  double MinCOkr;
  double AvgCOkr;
  double VarCOkr;
  double StdCOkr;
  double MaxCDzis;
  double MinCDzis;

  double AktC;
  double MaxC;
  double MinC;

  double MaxWOkr;
  double MinWOkr;
  double AvgWOkr;
  double VarWOkr;
  double StdWOkr;
  double MaxWDzis;
  double MinWDzis;

  double AktW;
  double MaxW;
  double MinW;

  void WysStruk();
  double MedTemp();
  double MedWilg();
  double MedCisn();
};




#endif // STRPOM_HPP
