#ifndef OKNOGLOWNE_H
#define OKNOGLOWNE_H

/*!
 * \file
 * \brief Zawiera definicję klasy OknoGlowne.
 *
 * Plik zawiera definicję klasy OknoGlowne. Jest to główne okno aplikacji.
 *
 */

#include <QMainWindow>
#include <QWidget>
#include <QApplication>
#include <QString>
#include <QTabWidget>
#include <QMenu>
#include <QMessageBox>
#include <QAction>
#include <QMutex>
#include <sstream>

#include "Zakladki.hpp"
#include "StrPom.hpp"
#include "BazaDanych.hpp"
#include "WatekBazyDanych.hpp"

/*!
 * \brief Przestrzeń nazw wykorzystywana przez QtDesigner'a. Nie korzystam z niego.
 */
namespace Ui {
class OknoGlowne;
}

/*!
 * \brief Definicja klasy OknoGlowne. (Najlepiej opisana*)
 *
 * Stanowi główne okno aplikacji. W tej klasie znajdują się menu, pasek statusowy, zakladki, klasa bazy danych i struktura pomiarowa.
 * Co bardzo ważne, to w tej klasie tworzony jest drugi wątek i przechowywany jest do niego wskaźnik.
 * Dostęp do tak ważnych wskaźników jak struktura pomiarowa, mutex, wątek jest możliwy za pomocą zdefiniowanych metod statycznych.
 */
class OknoGlowne : public QMainWindow{
    Q_OBJECT

public:
    /*!
     * \brief Konstruktor klasy OknoGlowne.
     */
    explicit OknoGlowne(QWidget *parent = nullptr);

    /*!
     * \brief Statyczna metoda zapewniająca dostęp do wskaźnika na strukturę pomiarową.
     */
    static StrPom* GetwPomiary();
    /*!
     * \brief Statyczna metoda zapewniająca dostęp do wskaźnika na handler wątku.
     */
    static WatekBazyDanych* GetwWatekBD();
    /*!
     * \brief Statyczna metoda zapewniająca dostęp do wskaźnika na mutex używany podczas korzystania ze struktury pomiarowej.
     */
    static QMutex* GetwMutex();
    /*!
     * \brief Statyczna metoda zapewniająca dostęp do wskaźnika na okno główne.
     */
    static OknoGlowne* GetswOknoG();

    /*!
      * \brief Destruktor klasy OknoGlowne.
      */
    ~OknoGlowne();

private:
    /*!
     * \brief Wskaźnik do widgetu Zakladki.
     */
    Zakladki    *_wZakladki;
    /*!
     * \brief Wskaźnik do obiektu BazaDanych.
     */
    BazaDanych   _Baza;
    /*!
     * \brief Wskaźnik do widgetu QMenu.
     */
    QMenu       *_wMenu;

    /*!
     * \brief Wskaźnik do akcji zamknięcia aplikacji.
     */
    QAction *_wAkcjaZamknij;
    /*!
     * \brief Wskaźnik do akcji pobierz dane z bazy i zapisz do pliku.
     */
    QAction *_wAkcjaPobierzDane;
    /*!
     * \brief Wskaźnik do akcji wyświetl informacje o aplikacji.
     */
    QAction *_wAkcjaInformacje;
    /*!
     * \brief Wskaźnik do akcji wyświetl pomoc.
     */
    QAction *_wAkcjaPomoc;

    /*!
     * \brief Pomocnicza metoda tworząca pasek statusu.
     */
    void UtworzPasekStatusu();
    /*!
     * \brief Pomocnicza metoda tworząca menu.
     */
    void UtworzMenu();
    /*!
     * \brief Pomocnicza metoda tworząca akcje.
     */
    void UtworzAkcje();
    /*!
     * \brief Pomocnicza metoda tworząca pasek zakładek.
     */
    void UtworzPasekZakladek();

    /*!
     * \brief Struktura pomiarowa służąca jako kontener na informacje z bazy danych.
     */
    StrPom           _Pomiary;
    /*!
     * \brief Wskaźnik na wątek obsługujący połączenia z bazą danych.
     */
    WatekBazyDanych* _wWatekBD;
    /*!
     * \brief Mutex zakładany podczas dostępu do struktury pomiarowej.
     */
    QMutex           _MutexGdyOdczytujePomiary;

    /*!
     * \brief Statyczne pole prywatne ze wskaźnikiem na strukturę pomiarową do łatwiejszego dostępu przez statyczny geter.
     */
    static StrPom*           sPomiary;
    /*!
     * \brief Statyczne pole prywatne ze wskaźnikiem na wątek obsługujący komunikację z bazą danych do łatwiejszego dostępu przez statyczny geter.
     */
    static WatekBazyDanych*  swWatekBD;
    /*!
     * \brief Statyczne pole prywatne ze wskaźnikiem na mutex zakładany podczas uzywania struktury pomiarowe do łatwiejszego dostępu przez statyczny geter.
     */
    static QMutex*           sMutex;
    /*!
     * \brief Statyczne pole prywatne ze wskaźnikiem na okno główne do łatwiejszego dostępu przez statyczny geter.
     */
    static OknoGlowne*       swOknoG;
public slots:
    /*!
   * \brief Slot do tworzenia napisu na pasku statusu po przełączeniu zakładek.
   */
  void OdbierzNapisStatusu(int index);
  /*!
   * \brief Slot do tworzenia napisu na pasku statusu przez podanie komunikatu.
   */
  void OdbierzNapisStatusu(QString tekst);
  /*!
   * \brief Slot uruchamiający ekran potwierdzenia zamknięcia aplikacji.
   */
  void CzyMoznaZamknac();
  void WyswietlInfo();
  void WyswietlPomoc();
};

#endif // OKNOGLOWNE_H
