#ifndef ZAKLADKI_HPP
#define ZAKLADKI_HPP

#include <QWidget>
#include <QTabWidget>
#include <sstream>
#include <QMutex>

#include "ZakladkaOgolne.hpp"
#include "ZakladkaTemp.hpp"
#include "ZakladkaCisn.hpp"
#include "ZakladkaWilg.hpp"
#include "StrPom.hpp"
#include "WatekBazyDanych.hpp"

class Zakladki: public QTabWidget{
  Q_OBJECT
public:
  Zakladki(QWidget *wRodzic);
  StrPom* getwStrPom() const;
private:
  ZakladkaTemp *_wZakladkaTemp;
  ZakladkaWilg *_wZakladkaWilg;
  ZakladkaCisn *_wZakladkaCisn;
  ZakladkaOgolne *_wZakladkaOgolne;
public slots:
  void GdyPobranoDane();
};

#endif /* ZAKLADKI_HPP */
