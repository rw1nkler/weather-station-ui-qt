#ifndef ZAKLADKACISN_HPP
#define ZAKLADKACISN_HPP

#include <QWidget>
#include <QGroupBox>
#include <QLabel>
#include <QLineEdit>
#include <QComboBox>
#include <QCalendarWidget>
#include <QCheckBox>
#include <QPushButton>

#include "WykresCisn.hpp"
#include "WykresWidok.hpp"
#include "StrPom.hpp"

using namespace std;

class ZakladkaCisn: public QWidget{
  Q_OBJECT
public:
  ZakladkaCisn(QWidget *wRodzic);
  void uaktualnij();
public slots:
    void GdyZatwierdzonoWybor(bool checked);
signals:
  void PrzekazStatus(QString tekst);
private:
  QGroupBox *_wWykCisnGroupBox;
  QGroupBox *_wInfoOgolneGroupBox;
  QGroupBox *_wDaneStatGroupBox;
  QGroupBox *_wPoleWyboruGroupBox;
  QGroupBox *_wPredykcjaGroubBox;
  WykresCisn *_wWykCisn;
  WykresWidok *_wWykCisnWidok;
  QCalendarWidget *_wKalendarzOD;
  QCalendarWidget *_wKalendarzDO;

  QLineEdit *_wEditMaxCisn;
  QLineEdit *_wEditMinCisn;
  QLineEdit *_wEditAktCisn;
  QLineEdit *_wEditMaxDzisCisn;
  QLineEdit *_wEditMinDzisCisn;

  QLineEdit *_wEditAvgCisn;
  QLineEdit *_wEditMedCisn;
  QLineEdit *_wEditStdCisn;
  QLineEdit *_wEditVarCisn;

  QComboBox *_wComboBoxZakres;
  QLabel *_wLabComboBoxZakres;

  QCheckBox *_wCheckBox;
  QLabel *_wLabCheckBox;
  QLabel *_wLabKalendarzOD;
  QLabel *_wLabKalendarzDO;
  QPushButton *_wZatwierdz;
};

#endif // ZAKLADKACISN_HPP
