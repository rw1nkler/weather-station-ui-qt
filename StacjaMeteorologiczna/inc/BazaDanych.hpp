#ifndef BAZADANYCH_HPP
#define BAZADANYCH_HPP

/*!
 * \file
 * \brief Zawiera definicję klasy BazaDanych (Najlepiej opisana*)
 *
 * Plik zawiera definicję klasy BazaDanych. Zawiera ona informację dotyczące połączenia
 * z bazą danych oraz metody, które umożliwiają pobranie danych z serwisu.
 *
 */

#include <QtSql>
#include "StrPom.hpp"

/*!
 * \brief Definicja klasy BazaDanych
 *
 * Zawiera ona informację o połączeniu z bazą danych znajdującą się na innym urządzeniu oraz informacje identyfikujące czujnik.
 * Dane te są niezbędne do pobierania informacji pogodowych. Klasa jest używana przez drugi wątek WatekBazyDanych,
 * który kontaktuje się z bazą danych w tle aktualizując informacje.
 */
class BazaDanych{
public:
  /*!
   * \brief Konstruktor bezparametryczny klasy BazaDanych.
   */
  BazaDanych();
  /*!
   * \brief Destruktor klasy BazaDanych.
   */
  ~BazaDanych();
  /*!
   * \brief Metoda pobierająca z bazy danych informacje o temperaturze.
   */
  int PobTemp(StrPom* wPomiary);
  int PobCisn(StrPom* wPomiary);
  int PobWilg(StrPom *wPomiary);
  /*!
   * \brief Metoda pobierająca z bazy danych informacje o ciśnieniu.
   */
  int PobCisn_Dni(StrPom *wPomiary);
  int PobCisn_Data(StrPom *wPomiary);
  int PobWilg_Dni(StrPom *wPomiary);
  int PobWilg_Data(StrPom *wPomiary);
private:
  /*!
   * \brief Prywatna metoda pomocnicza pobierająca informacje o temperaturze n dni wstecz.
   */
  int PobTemp_Dni(StrPom* wPomiary);
  /*!
   * \brief Prywatna metoda pomocnicza pobierająca informacje o temperaturze w zadanym zakresie dat.
   */
  int PobTemp_Data(StrPom* wPomiary);
  /*!
   * \brief Pole prywatne przechowujące informacje niezbędne do połączenia z bazą danych.
   */
  QSqlDatabase db;
  /*!
   * \brief Pole prywatne przechowujące informacje o ID czujnika, którego dane pobieramy.
   */
  int _CzujnikID;
};

#endif // BAZADANYCH_HPP
