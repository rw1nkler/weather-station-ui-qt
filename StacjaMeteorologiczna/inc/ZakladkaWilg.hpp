#ifndef ZAKLADKAWILG_HPP
#define ZAKLADKAWILG_HPP

#include <QWidget>
#include <QGroupBox>
#include <QLabel>
#include <QLineEdit>
#include <QComboBox>
#include <QCalendarWidget>
#include <QCheckBox>
#include <QPushButton>

#include "WykresWilg.hpp"
#include "WykresWidok.hpp"


using namespace std;

class ZakladkaWilg: public QWidget{
    Q_OBJECT
public:
    ZakladkaWilg(QWidget *wRodzic);
    void uaktualnij();
public slots:
    void GdyZatwierdzonoWybor(bool checked);
signals:
    void PrzekazStatus(QString tekst);
  private:
    QGroupBox *_wWykWilgGroupBox;
    QGroupBox *_wInfoOgolneGroupBox;
    QGroupBox *_wDaneStatGroupBox;
    QGroupBox *_wPoleWyboruGroupBox;
    QGroupBox *_wPredykcjaGroubBox;
    WykresWilg *_wWykWilg;
    WykresWidok *_wWykWilgWidok;
    QCalendarWidget *_wKalendarzOD;
    QCalendarWidget *_wKalendarzDO;

    QLineEdit *_wEditMaxWilg;
    QLineEdit *_wEditMinWilg;
    QLineEdit *_wEditAktWilg;
    QLineEdit *_wEditMaxDzisWilg;
    QLineEdit *_wEditMinDzisWilg;

    QLineEdit *_wEditAvgWilg;
    QLineEdit *_wEditMedWilg;
    QLineEdit *_wEditStdWilg;
    QLineEdit *_wEditVarWilg;

    QComboBox *_wComboBoxZakres;
    QLabel *_wLabComboBoxZakres;

    QCheckBox *_wCheckBox;
    QLabel *_wLabCheckBox;
    QLabel *_wLabKalendarzOD;
    QLabel *_wLabKalendarzDO;
    QPushButton *_wZatwierdz;
};


#endif // ZAKLADKAWILG_HPP
