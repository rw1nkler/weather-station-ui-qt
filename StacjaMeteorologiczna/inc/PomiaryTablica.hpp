#ifndef POMIARYTABLICA_HPP
#define POMIARYTABLICA_HPP

#include <QTime>
#include <QDate>

typedef struct{
public:
  QTime Czas;
  QDate Data;
  double Wartosc;
}WpisPomiarowy;


class PomiaryTablica{
public:

  PomiaryTablica();
  ~PomiaryTablica();

  int GetSize() const;
  void ReallocateTab(int size);

  WpisPomiarowy& Tab(int w);
private:
  WpisPomiarowy* _tab;
  int            _size;
};
#endif // POMIARYTABLICA_HPP
