#ifndef WYKRES_HPP
#define WYKRES_HPP

#include <QWidget>
#include <QChartGlobal>
#include <QChart>
#include <QChartView>
#include <QSplineSeries>
#include <QLegend>
#include <QPalette>
#include <QString>
#include <QDateTime>
#include <QDateTimeAxis>
#include <QValueAxis>
#include <QObject>

#include "PomiaryTablica.hpp"
#include "StrPom.hpp"

QT_CHARTS_USE_NAMESPACE

/*!
 * \brief Definicja klasy WykresTemp
 *
 * Umożliwia manipulowanie parametrami wykresu. Zapewnia mechanizmy aktualizacji danych na wykresie. Służy do wyliczania predykcji wartości.
 */
class WykresTemp: public QObject{
  Q_OBJECT
public:
  WykresTemp(PomiaryTablica *wPomiary, IleDni Zakres);
  ~WykresTemp();

  QString& getNazwa() const;
  void setNazwa(QString nazwa);
  QChart* getWykres() const;
  void setTitle(QString title);
  /*!
   * \brief Metoda wykorzystywana do aktualizacji danych na wykresie.
   */
  void uaktualnij();
private:
  /*!
   * \brief Przechowuje nazwę wykresu
   */
  QString *_wNazwa;
  /*!
   * \brief Przechowuje wskaźnik do obiektu Qt wykorzystywanego do rysowania wykresu.
   */
  QChart *_wWykres;
  /*!
   * \brief Wskaźnik na strukturę z pomiarami dotyczącymi temperatury
   */
  PomiaryTablica *_wPomiary;
  /*!
   * \brief Wskaźnik na serię danych pokazująca informacje o temperaturze na wykresie
   */
  QLineSeries *_wSeriaDanych;
  /*!
   * \brief Wskaźnik n serię z predykcją temperatury
   */
  QLineSeries *_wPredykcja;
  QDateTimeAxis *_osX;
  QValueAxis *_osY;
  IleDni _Zakres;

public slots:
  /*!
   * \brief Slot uruchamiany kiedy urzytkownik zarzyczy sobie predykcji wartości
   */
  void GdyChcePredykcje(int stan);
};


#endif /* WYKRES_HPP */
