#-------------------------------------------------
#
# Project created by QtCreator 2018-05-12T21:10:51
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

QT += sql
QT += charts

TARGET = StacjaMeteorologiczna
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

QMAKE_CXXFLAGS+=-std=c++11

OBJECTS_DIR=./obj
MOC_DIR=./moc
INCLUDEPATH=./inc


SOURCES += \
    src/main.cpp \
    src/ZakladkaOgolne.cpp \
    src/ZakladkaTemp.cpp \
    src/Zakladki.cpp \
    src/BazaDanych.cpp \
    src/OknoGlowne.cpp \
    src/WatekBazyDanych.cpp \
    src/WykresWidok.cpp \
    src/StrPom.cpp \
    src/WykresTemp.cpp \
    src/PomiaryTablica.cpp \
    src/WykresCisn.cpp \
    src/ZakladkaCisn.cpp \
    src/WykresWilg.cpp \
    src/ZakladkaWilg.cpp

HEADERS += \
    inc/ZakladkaOgolne.hpp \
    inc/ZakladkaTemp.hpp \
    inc/Zakladki.hpp \
    inc/BazaDanych.hpp \
    inc/WatekBazyDanych.hpp \
    inc/WykresWidok.hpp \
    inc/StrPom.hpp \
    inc/WykresTemp.hpp \
    inc/PomiaryTablica.hpp \
    inc/WykresCisn.hpp \
    inc/ZakladkaCisn.hpp \
    inc/OknoGlowne.hpp \
    inc/WykresWilg.hpp \
    inc/ZakladkaWilg.hpp

FORMS += \
        oknoglowne.ui
